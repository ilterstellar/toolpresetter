﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using ToolDataLibraryAsync.Models;
using ToolDataLibraryAsync.Data_Access.SQLite;
using System.Text.Json;
using System.Threading.Tasks;
using System.IO;
using System.Collections.ObjectModel;
using System.Net;
using ToolDataLibraryAsync.ViewModels;
using System.Windows;

namespace ToolDataLibraryAsync.ToolCenterComm
{
    public class ToolCenterCommunication
    {
        public string ServerAddress;
        public int Port;
        private ToolDataDal _toolDataDal;
        public TcpClient client;
        private NetworkStream stream;
        public bool ConnectionStatus;
        private ObservableCollection<DbToolModel> retrievedTools;
        public DbToolModel retrievedTool;
        public ToolCenterCommunication()
        {
            _toolDataDal = new ToolDataDal();
            //Task.Run(() => Connect());
        }
        public void Connect()
        {
            try
            {
                
                if(client != null)
                {
                    IPAddress ipAddress = ((IPEndPoint)client.Client.RemoteEndPoint).Address;
                    int port = ((IPEndPoint)client.Client.RemoteEndPoint).Port;

                    if (!(port.Equals(Port)) || !(ipAddress.Equals(IPAddress.Parse(ServerAddress))) || client.Connected == false)
                    {
                        client.Dispose();
                    }
                }
                    
                
                if (client == null || client.Connected==false) {

                    client = new TcpClient();
                    client.ConnectAsync(ServerAddress, Port).Wait(1000);
                    stream = client.GetStream();
                    
                    //Task sendTask = SendData();
                    ConnectionStatus = client.Connected;
                    //ListenData().Wait(250);
                }
                else
                {
                    ConnectionStatus = client.Connected;
                }
                
            }
            catch (SocketException ex) when (ex.SocketErrorCode == SocketError.ConnectionRefused)
            {
                MessageBox.Show(ex.Message);
                ConnectionStatus = false; 
                client = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ConnectionStatus = false;
                client = null;
            }
        }
        public async Task SendIdFromRFID(int Id)
        {
            try
            {
                if (client != null && client.Connected)
                {
                    byte[] data = Encoding.UTF8.GetBytes(Id.ToString());
                    await stream.WriteAsync(data, 0, data.Length);
                    ListenData().Wait(1000);
                }
                else
                {
                    Reconnect();
                    if (client != null && client.Connected)
                    {
                        byte[] data = Encoding.UTF8.GetBytes(Id.ToString());
                        await stream.WriteAsync(data, 0, data.Length);
                        ListenData().Wait(1000);
                        ConnectionStatus = true;
                    }
                }
                   
            }
            catch (IOException ex) when (ex.InnerException is ObjectDisposedException)
            {
                ConnectionStatus = false;
                Reconnect();
            }
            catch (Exception ex)
            {
                stream = client.GetStream();
                ConnectionStatus = false;
            }
            
        }
        public void SendData()
        {
            
                try
                {
                    if (client != null && client.Connected)
                    {
                        List<Tool> tools = _toolDataDal.GetAllTools().Result;
                        string jsonTools = JsonSerializer.Serialize(tools);
                        byte[] jsonData = Encoding.UTF8.GetBytes(jsonTools);
                        stream.Write(jsonData, 0, jsonData.Length);
                        var boo = ListenData().Wait(1500);
                        if (!boo)
                        {
                            Reconnect();
                        };
                        
                    }
                    else
                    {
                    Reconnect();
                    }
                }
                catch (Exception ex)
                {
                    
                        Reconnect();   
                }
            
        }

        private void Reconnect()
        {
            try
            {
                client = new TcpClient(ServerAddress, Port);
                stream = client.GetStream();
                ConnectionStatus = client.Connected;
            }
            catch (Exception ex)
            {                
                ConnectionStatus = false;
            }
           
        }
        public async Task ListenData()
        {
          
                
                try
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        byte[] buffer = new byte[1024];
                        int bytesRead;
                        

                    while (true)
                        {
                            bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length);
                            if (bytesRead > 0)
                            {
                                await memoryStream.WriteAsync(buffer, 0, bytesRead);

                                
                                

                            }
                            if (bytesRead < buffer.Length)
                            {
                                break;
                            }
                        }
                        var json = Encoding.UTF8.GetString(memoryStream.ToArray());

                    
                    if (json.Contains("["))
                        {

                            retrievedTools = JsonSerializer.Deserialize<ObservableCollection<DbToolModel>>(json);
                            
                        }
                        else
                        {
                            retrievedTool = JsonSerializer.Deserialize<DbToolModel>(json);
                            RFIDViewModel.Instance.DbToolModel = retrievedTool;
                        }
                        json = String.Empty;
                        //string response = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                        
                        ConnectionStatus = true;
                    }
                      
                }
                catch (IOException ex) when (ex.InnerException is ObjectDisposedException)
                {
                    ConnectionStatus = false;
                    
                   // stream = client.GetStream();
                    //await ListenData();

                }
                catch (Exception ex)
                {
                    ConnectionStatus = false;
                    
                    //stream = client.GetStream();
                   // await ListenData();
                }
            }
        

        public ObservableCollection<DbToolModel> GetILoaderToolList()
        {
            return retrievedTools;
        }
    }

}


