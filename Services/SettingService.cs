﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolDataLibraryAsync.Data_Access.SQLite.SettingsDb;
using ToolDataLibraryAsync.Models;
using ToolDataLibraryAsync.ViewModels;

namespace ToolDataLibraryAsync.Services
{
    public class SettingService:ViewModelBase
    {
        private static SettingService _instance;
        private int _selectedFontSize;
        private string _selectedStyle;
        private string _selectedPort;
        private string _selectedLanguage;
        private int _selectedTimeInterval;
        private readonly string mPath = "Software\\ToolPresetter";
        UISettings UISettings = new UISettings();
        public static SettingService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SettingService();
                    
                }
                return _instance;
            }
        }

        public SettingService()
        {
            var settingsDal = new SettingsDal();
            _selectedFontSize = Convert.ToInt32(settingsDal.GetSetting("FontSize"));
            _selectedStyle = (string)settingsDal.GetSetting("Style");
            _selectedPort = (string)settingsDal.GetSetting("SerialPort");
            _selectedLanguage = (string)settingsDal.GetSetting("Language");
            _selectedTimeInterval = Convert.ToInt32(settingsDal.GetSetting("TimeInterval"));
            //if (IsRegeditExist(mPath))
            //{
            //    UISettings.Language = RegeditRead(mPath, nameof(UISettings.Language)).ToString();
            //    UISettings.FontSize = Convert.ToUInt16(RegeditRead(mPath, nameof(UISettings.Language)).ToString());
            //    UISettings.Style = RegeditRead(mPath, nameof(UISettings.Language)).ToString();
            //    UISettings.Port = RegeditRead(mPath, nameof(UISettings.Language)).ToString();
            //    UISettings.TimeInterval = Convert.ToUInt16(RegeditRead(mPath, nameof(UISettings.Language)).ToString());
            //}

        }
        public void Save()
        {
            RegeditWrite(mPath, nameof(UISettings.Language), UISettings.Language);
            RegeditWrite(mPath, nameof(UISettings.FontSize), UISettings.FontSize);
            RegeditWrite(mPath, nameof(UISettings.Style), UISettings.Style);
            RegeditWrite(mPath, nameof(UISettings.Port), UISettings.Port);
            RegeditWrite(mPath, nameof(UISettings.TimeInterval), UISettings.TimeInterval);

        }
        public string SelectedLanguage
        {
            get { return _selectedLanguage; }
            set
            {
                _selectedLanguage = value;
                
                OnPropertyChanged();
            }
        }
        public int SelectedTimeInterval
        {
            get { return _selectedTimeInterval; }
            set
            {
                _selectedTimeInterval = value;
               
                OnPropertyChanged();
                
            }
        }
        public int SelectedFontSize
        {
            get { return _selectedFontSize; }
            set
            {
                _selectedFontSize = value;
                // Call a method to notify subscribers about the font size change
                NotifyFontSizeChanged();
            }
        }
        public string SelectedPort
        {
            get { return _selectedPort; }
            set
            {
                _selectedPort = value;
                OnPropertyChanged();
            }
        }
        public string SelectedStyle
        {
            get { return _selectedStyle; }
            set
            {

                _selectedStyle = value;
                NotifyStyleChanged();
                
            }
        }

        public event EventHandler FontSizeChanged;
        public event EventHandler StyleChanged;

        protected virtual void NotifyStyleChanged()
        {
            StyleChanged?.Invoke(this, EventArgs.Empty);
        }
        protected virtual void NotifyFontSizeChanged()
        {
            FontSizeChanged?.Invoke(this, EventArgs.Empty);
        }
        public bool IsRegeditExist(string path) => Registry.CurrentUser.OpenSubKey(path) != null;

        public object RegeditRead(string path, string key)
        {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(path);
            if (registryKey == null)
                return (object)null;
            try
            {
                return registryKey.GetValue(key);
            }
            catch
            {
                return (object)null;
            }
        }

        public bool RegeditWrite(string path, string key, object value)
        {
            try
            {
                using (RegistryKey currentUser = Registry.CurrentUser)
                {
                    using (RegistryKey subKey = currentUser.CreateSubKey(path))
                        subKey.SetValue(key, value);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }

}
