﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToolDataLibraryAsync.Views;

namespace ToolDataLibraryAsync
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {   //Closes All Windows
            Closing += MainWindow_Closing;
            
            SplashScreenWindow splashScreen = new SplashScreenWindow();
            splashScreen.Show();
            InitializeComponent();
            
            Thread.Sleep(1000);

            splashScreen.Close();
        }
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Get all open windows
            var windows = Application.Current.Windows.OfType<Window>().ToList();

            // Close all windows except the main window
            foreach (var window in windows)
            {
                if (window != this)
                {
                    window.Close();
                }
            }
        }
    }
}
