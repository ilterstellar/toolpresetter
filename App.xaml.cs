﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ToolDataLibraryAsync.Properties;
using ToolDataLibraryAsync.RabbitMQ;
namespace ToolDataLibraryAsync
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {

        }
        protected override async void OnStartup(StartupEventArgs e)
        {
            IoC.InitializeDb();
            
        }
    }
}
