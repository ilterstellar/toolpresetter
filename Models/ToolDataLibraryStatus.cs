﻿namespace ToolDataLibraryAsync.Models
{
    public class ToolDataLibraryStatus
    {
        public string Name { get; set; }
       public string LibraryPath { get; set; }
       public string ToolPath { get; set; }
       public string ToolDbPath { get; set; }
       public bool LibraryPathBool { get; set; }
       public bool ToolPathBool { get; set; }
       public bool TimeBool { get; set; }
       public string Lang { get; set; } 
       public int TimeInterval { get; set; }
    }
}
