﻿using ToolDataLibraryAsync.ViewModels;

namespace ToolDataLibraryAsync.Models
{
    public class Language:ViewModelBase
    {
        private string _content;

        public string Content
        {
            get { return _content; }
            set { _content = value; OnPropertyChanged();}
        }

        private string _tag;

        public string Tag
        {
            get
            {
                return _tag;
            }
            set
            {
                _tag=value; OnPropertyChanged();
            }
        }
    }
}
