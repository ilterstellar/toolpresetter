﻿
namespace ToolDataLibraryAsync.Models
{
    public class MachineData
    {
        public int IdNumber { get; set; }
        public string Tool { get; set; }

        public double Radius { get; set; }
        public decimal Diameter { get; set; }
        public decimal Length { get; set; }
        public string ToolType { get; set; }
        public string ToolUserType { get; set; }
        public int Permanent { get; set; }
        public string UnitsDiameter { get; set; }
        public decimal Angle { get; set; }
        public decimal ProfileRadius { get; set; }
        public decimal UpperRadius { get; set; }
        public int NumTeeth { get; set; }
        public string Description { get; set; }
        public decimal TaperAngle { get; set; }
        public decimal ShankDiameter { get; set; }
        public string UnitsLength { get; set; }
        public int TotalLength { get; set; }
        public int ShoulderLength { get; set; }
        public int Startshoulderlength { get; set; }
        public int TipLength { get; set; }
        public decimal CuttingLength { get; set; }
        public int HLength { get; set; }
        public string Material { get; set; }
        public string UnitsFeedSpin { get; set; }
        public string FType { get; set; }
        public decimal FeedXY { get; set; }
        public decimal FeedZ { get; set; }
        public decimal FeedFinish { get; set; }
        public string SType { get; set; }
        public decimal Spin { get; set; }
        public decimal SpinFinish { get; set; }
        public string FeedZPenetration { get; set; }
        public int FeedLeadIn { get; set; }
        public int FeedLeadOut { get; set; }
        public int FeedLink { get; set; }
        public string ToolName { get; set; }
        public string ToolGroup { get; set; }
        public string HolderName { get; set; }
        public string GroupHolderName { get; set; }
        public string Direction { get; set; }
        public int ChamferLength { get; set; }
        public decimal TipDiameter { get; set; }
        public decimal ShoulderDiameter { get; set; }
        public decimal ShoulderAngle { get; set; }
        public string Message1 { get; set; }
        public string Message2 { get; set; }
        public string Message3 { get; set; }
        public string Message4 { get; set; }
        public string Message5 { get; set; }
        public int FloodCoolant { get; set; }
        public int MistCoolant { get; set; }
        public int HighPressureCoolant { get; set; }
        public int LowPressureCoolant { get; set; }
        public int ThroughHighPressureCoolant { get; set; }
        public int ThroughLowPressureCoolant { get; set; }
        public int AirBlastCoolant { get; set; }
        public int MinimumQuantityLubricationCoolant { get; set; }
        public int MinimumQuantityLubricationValue { get; set; }
        public int Rough { get; set; }
        public string SpinDirection { get; set; }
        public int SpinLimit { get; set; }
        public string ThreadingStandard { get; set; }
        public string ThreadStandardByUser { get; set; }
        public string ThreadingTableStandard { get; set; }
        public int NumThreads { get; set; }
        public decimal Pitch { get; set; }
        public string PitchUnit { get; set; }
        public string CNCName { get; set; }
    }
}
