﻿
using System;

namespace ToolDataLibraryAsync.Models
{
    [Serializable]
    public class Tool
    {
        
        public int Id { get; set; }
        public string Number { get; set; }

        public string Name { get; set; }

        public double Radius { get; set; }
        public double Diameter { get; set; }
        public double CoordinateZ { get; set; }

        public double TheoreticalRadius { get; set; }
        public double TheoreticalZ { get; set; }
        public string LifeOf { get; set; }
        public double CutterEdgeRadius { get; set; }
        public int CutterEdgeOrient { get; set; }
        public int Type { get; set; }
        public string CNCName { get; set; }
    }
}
