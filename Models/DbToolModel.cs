﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using ToolDataLibraryAsync.Enums;
using ToolDataLibraryAsync.ViewModels;

namespace ToolDataLibraryAsync.Models
{
    public class DbToolModel:ViewModelBase
    {
        private int _id;
        public int Id {
            get => _id;
            set
            {
                if (_id != value)
                {
                    _id = value;
                    


                }
            }
        }
        private int _functionId;
        public int FunctionId
        {
            get => _functionId;
            set
            {
                    _functionId = value;
                
            }
        }
        private string _description;
        public string Description
        {
            get => _description;
            set
            {
                
                    _description = value;
                


            }
        }
        public int Pot { get; set; } = 0;
        public EnumDbToolInOut Status { get; set; }
        public EnumDbToolMaterial Material { get; set; }
        public double Diameter { get; set; }
        public double DiameterOffset { get; set; }
        public double DiameterOffsetWear { get; set; }
        public double Lenght { get; set; }
        public double LenghtOffset { get; set; }
        public double LenghtOffsetWear { get; set; }
        public int ToolLife { get; set; }
        public int ActualToolLife { get; set; }
        public int ToolLifeWarning { get; set; }
        public int MaxLoadRate { get; set; }
        public int MinLoadRate { get; set; }
        public EnumToolLifeUnit LifeUnit { get; set; } = EnumToolLifeUnit.Minute;
        public bool IsBtsEnable { get; set; } = false;
    }
}
