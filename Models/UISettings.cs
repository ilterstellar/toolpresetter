﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolDataLibraryAsync.Models
{
    public class UISettings
    {
        public string uISettings { get; set; }
        public int FontSize { get; set; }
        public string Style { get; set; }
        public string SerialPort { get; set; }
        public string Language { get; set; }
        public int TimeInterval { get; set; }
        public string HostName { get; set; }
        public int Port { get; set; }
        public int MQPort { get; set; }
        
    }
}
