﻿namespace ToolDataLibraryAsync.ViewModels
{
    public class MainViewModel:ViewModelBase
    {
        public MainViewModel()
        {
            SelectedViewModel = new UserControlViewModel();
        }
        private ViewModelBase _selectedViewModel;
        public ViewModelBase SelectedViewModel
        {
            get { return _selectedViewModel; }
            set
            {
                _selectedViewModel = value;
                OnPropertyChanged(nameof(SelectedViewModel)); }
        }
    }
}