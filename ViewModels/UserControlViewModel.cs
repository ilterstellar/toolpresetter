﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using ToolDataLibraryAsync.Properties;
using ToolDataLibraryAsync.Commands;
using ToolDataLibraryAsync.Data_Access.SQLite;
using ToolDataLibraryAsync.Models;
using MachineDataDal = ToolDataLibraryAsync.Data_Access.SQLite.MachineDataDal;
using Path = System.IO.Path;
using System.Windows.Forms;
using ToolDataLibraryAsync.Data_Access.SQLite.SettingsDb;
using ToolDataLibraryAsync.Services;
using ToolDataLibraryAsync.Views;
using MessageBox = System.Windows.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using System.Timers;


namespace ToolDataLibraryAsync.ViewModels
{
    
    public class UserControlViewModel : ViewModelBase
    {
        private SerialPort RFID;
        private readonly MachineDataDal _machineDataDal;
        private readonly ToolDataDal _toolDataDal;
        private readonly SettingsDal _settingsDal;
        private readonly StatusDal _statusDal;
        
        private static readonly string WorkingDirectory = Environment.CurrentDirectory;
        private static readonly string ProjectDirectory = Directory.GetParent(WorkingDirectory)?.Parent?.FullName;
        private static readonly string ConfigFilePath = $"{ProjectDirectory}\\App.config";
        
        private string _LibraryPath;
        private string _filesPath;
        private string _path;

        private bool _timeBool;
        private bool _inverseTimeLoop;
        
        private System.Timers.Timer _timer;
        // Constructor for UserControlViewModel
        public UserControlViewModel()
        {
            
            _machineDataDal = new MachineDataDal();
            _toolDataDal = new ToolDataDal();
            _statusDal = new StatusDal();
            _settingsDal = new SettingsDal();
            //Localization
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo((string)(_settingsDal.GetSetting("Language")));
            GetResources();
            Languages = new ObservableCollection<Language>() { new Language { Content = cbxEnglish, Tag = "en-US" }, new Language { Content = cbxTurkish, Tag = "tr-TR" } };
            _selectedLanguage = (string)_settingsDal.GetSetting("Language");
            _filesPath = (string)_statusDal.GetStatus("ToolPath");
            DataGrid1Data = new ObservableCollection<MachineData>(_machineDataDal.GetAllMachineData().Result);
            DataGrid2Data = new ObservableCollection<Tool>(_toolDataDal.GetAllTools().Result);

            SavePathCommand = new AsyncRelayCommand(SaveLibraryPath);
            SaveToolPathCommand = new AsyncRelayCommand(async() =>await SaveToolPath());
            CheckUpdateDataGrid2Command = new AsyncRelayCommand(CheckUpdateData2);
            CreateTableCommand = new AsyncRelayCommand(CreateTable);
            UpdateGrids_User();
            TimeCollection = new ObservableCollection<int>()
            {
                5,10,15,20,25,30,60,120,180,240
            };
            Thread.Sleep(250);
            _timeBool = false;
            _inverseTimeLoop = !_timeBool;
            _timeInterval = Convert.ToInt32(_settingsDal.GetSetting("TimeInterval"));
            
           
            Duration = new Duration(TimeSpan.FromSeconds(_timeInterval));
            _timeBool = !_timeBool;
            _timeBool=!_timeBool;
            OpenPopupCommand = new AsyncRelayCommand(OpenPopup);
            ClosePopupCommand = new AsyncRelayCommand(ClosePopup);
            OpenRfidWindowCommand = new RelayCommand(RfidReader);
            OpenSettingsCommand = new RelayCommand(OpenSettings);
            SettingService.Instance.FontSizeChanged += OnFontSizeChanged;
            SettingService.Instance.StyleChanged += OnStyleChanged;
            
            FontSize = Convert.ToInt32(_settingsDal.GetSetting("FontSize"));
            Style = (string)_settingsDal.GetSetting("Style");
            HostName = (string)_settingsDal.GetSetting("HostName");
            Port = Convert.ToInt32( _settingsDal.GetSetting("Port"));
            Time();
            
            Connect();
            Connection = IoC.Communication.ConnectionStatus;
        }

        private void OnFontSizeChanged(object sender, EventArgs e)
        {
            FontSize = Settings.Instance.SelectedFontSize;
        }
        private void OnStyleChanged(object sender, EventArgs e)
        {
            if (SettingService.Instance.SelectedStyle == "White")
            {
                Style = "White";
            }
            if (SettingService.Instance.SelectedStyle == "Blue")
            {
                Style = "Blue";
            }
            if (SettingService.Instance.SelectedStyle == "Green")
            {
                Style = "Green";
            }
            if (SettingService.Instance.SelectedStyle == "Violet")
            {
                Style = "Violet";
            }

        }
        private bool _connection;
        public bool Connection
        {
            get { return _connection; }
            set { _connection = value; OnPropertyChanged(nameof(Connection)); }
        }
       
        private string _hostName;
        public string HostName
        {
            get { return _hostName; }
            set { _hostName = value; OnPropertyChanged(nameof(HostName));}
        }
        private int _port;
        public int Port
        {
            get { return _port; }
            set { _port = value; OnPropertyChanged(nameof(Port)); }
        }
        private string _txtSettings;
        public string txtSettings
        {
            get { return _txtSettings; }
            set
            {
                _txtSettings = value;
                OnPropertyChanged(nameof(txtSettings));
            }
        }
        private string _txtScanTag;
        public string txtScanTag
        {
            get { return _txtScanTag; }
            set
            {
                _txtScanTag = value;
                OnPropertyChanged(nameof(txtScanTag));
            }
        }
        private string _style;

        public string Style
        {
            get { return _style; }
            set
            {
                _style=value;
                OnPropertyChanged(nameof(Style));
                _settingsDal.UpdateSetting("Style", _style);
            }
        }

        private int _fontSize;

        public int FontSize
        {
            get { return _fontSize; }
            set
            {
                _fontSize = value;
                OnPropertyChanged(nameof(FontSize)); 
                _settingsDal.UpdateSetting("FontSize", _fontSize);
                
            }
            
        }

        #region Variables

        private string _msgFile;

        public string msgFile
        {
            get => _msgFile;
            set { _msgFile = value; OnPropertyChanged(); }
        }


        private static string _cncName;

        private string _cncText;
        public string CNCText
        {
            get => _cncText;
            set
            {
                _cncText = value;
                OnPropertyChanged(nameof(CNCText));
                _cncName = _cncText;

            }
        }
        public string ToolDbPath
        {
            get => _toolDbPath;
            set
            {
                _toolDbPath = value;
                OnPropertyChanged();
            }
        }
        private bool _isPopupOpen;
        public bool IsPopupOpen
        {
            get => _isPopupOpen;
            set
            {
                _isPopupOpen = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<int> _timeCollection;

        public ObservableCollection<int> TimeCollection
        {
            get => _timeCollection;
            set
            {
                _timeCollection = value;
                OnPropertyChanged(nameof(TimeCollection));

            }
        }
       
        private int _progress = 0;
        public int Progression
        {
            get => _progress;
            set { _progress = value; OnPropertyChanged(); }
        }

        public bool InverseTimeLoop
        {
            get => _inverseTimeLoop;
            set
            {
                _inverseTimeLoop = value;
                OnPropertyChanged();
            }
        }

        private Duration _duration;
        public Duration Duration
        {
            get => _duration;
            set
            {
                _duration =
                    value;
                OnPropertyChanged(nameof(Duration));
            }
        }
        private string _txtProgression;

        public string txtProgression
        {
            get => _txtProgression;
            set
            {
                _txtProgression = value;
                OnPropertyChanged();
            }
        }
        private string _buttonContent;
        public string ButtonContent
        {
            get => _buttonContent;
            set
            {
                _buttonContent = value;
                OnPropertyChanged(nameof(ButtonContent));
            }
        }

        private string _help;

        public string Helper
        {
            get => _help;
            set { _help = value; OnPropertyChanged(); }

        }
        private string _libraryPath;

        public string LibraryPath
        {
            get => _libraryPath;
            set
            {
                _libraryPath = value;
                OnPropertyChanged();
            }
        }
        private string _toolPath;

        public string ToolPath
        {
            get => _toolPath;
            set
            {
                _toolPath = value;
                OnPropertyChanged();
            }
        }

        private string _toolDbPath;
        private string _txtUpdate;

        public string txtUpdate
        {
            get => _txtUpdate;
            set { _txtUpdate = value; OnPropertyChanged(nameof(txtUpdate)); }
        }
        private string _txtTimer;
        public string txtTimer
        {
            get => _txtTimer;
            set { _txtTimer = value; OnPropertyChanged(nameof(txtTimer)); }
        }

        private string _txtHelper;

        public string txtHelper
        {
            get => _txtHelper;
            set
            {
                _txtHelper = value; OnPropertyChanged(nameof(txtHelper));
            }
        }
        #endregion

        #region Localization
        private string _txtLanguage;

        public string txtLanguage
        {
            get => _txtLanguage;
            set { _txtLanguage = value; OnPropertyChanged(nameof(txtLanguage)); }
        }
        private ObservableCollection<Language> _languages;
        public ObservableCollection<Language> Languages
        {
            get => _languages;
            set
            {
                _languages = value;
                Language();
                OnPropertyChanged(nameof(Languages));

            }
        }
        private CultureInfo _currentCulture;
        private void Language()
        {
            switch (Convert.ToString(_currentCulture))
            {
                case "tr-TR":
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("tr-TR");
                    GetResources();
                    Languages[0].Content = cbxEnglish;
                    Languages[1].Content = cbxTurkish;
                    //Languages[2].Content = cbxItalian;


                    break;

                case "en-US":
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
                    GetResources();
                    Languages[0].Content = cbxEnglish;
                    Languages[1].Content = cbxTurkish;
                    //Languages[2].Content = cbxItalian;

                    break;

                    //case "it-IT":
                    //    Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("it-IT");
                    //    GetResources();
                    //    Languages[0].Content = cbxEnglish;
                    //    Languages[1].Content = cbxTurkish;
                    //    Languages[2].Content = cbxItalian;

                    //    break;

            }
        }

        

        private void GetResources()
        {
            LibraryPath = Resources.PathOfLibrary;
            ToolDbPath = Resources.DataBase;
            ToolPath = Resources.PathOfTools;
            Helper = Resources.Helper;
            txtTimer = Resources.txtTimer;
            txtLanguage = Resources.txtLanguage;
            txtHelper = Resources.txtHelper;
            txtUpdate = Resources.txtUpdate;
            cbxEnglish = Resources.cbxEnglish;
            cbxTurkish = Resources.cbxTurkish;
            //cbxItalian = Resources.cbxItalian;
            txtProgression = Resources.txtProgression;

            msgFile = Resources.msgFile;
            txtSettings = Resources.txtSettings;
            txtScanTag = Resources.txtScanTag;
        }
        private string _cbxEnglish;
        public string cbxEnglish
        {
            get => _cbxEnglish;
            set
            {
                _cbxEnglish = value;
                OnPropertyChanged(nameof(cbxEnglish));
            }
        }
        //private string _cbxItalian;

        //public string cbxItalian
        //{
        //    get { return _cbxItalian; }
        //    set
        //    {
        //        _cbxItalian = value;
        //        OnPropertyChanged(nameof(cbxItalian));
        //    }
        //}
        private string _cbxTurkish;

        public string cbxTurkish
        {
            get => _cbxTurkish;
            set
            {
                _cbxTurkish = value;
                OnPropertyChanged(nameof(cbxTurkish));
            }
        }
        private string _selectedLanguage;

        public string SelectedLanguage
        {
            get => _selectedLanguage;
            set
            {
                _selectedLanguage = value;
                _currentCulture = new CultureInfo(_selectedLanguage);
                SaveToConfig(var: "Lang", value: _selectedLanguage);
                _settingsDal.UpdateSetting("Language",_selectedLanguage);
                OnPropertyChanged(nameof(SelectedLanguage));

                Language();
            }

        }
        #endregion

        #region Time
        private void Time()
        {
            _timeInterval = Convert.ToInt32(_settingsDal.GetSetting("TimeInterval"));
            _timer = new System.Timers.Timer(_timeInterval * 1000);
            _timer.AutoReset = true;

            switch (_timeBool)
            {
                case true:
                    Settings.Instance.TimeFlag = false;
                    _timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                    _timer.Start();
                    break;
                case false:
                    Settings.Instance.TimeFlag = true;
                    _timer.Stop(); 
                    _timer.Elapsed -= new ElapsedEventHandler(OnTimedEvent);
                    _timer.Close();
                    SaveToConfig("TimeBool", _timeBool);
                    break;
            }
        }




    private async void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            if (_timeBool) 
            {
                await Task.Run(() => ReadFromFile_Continuous());
                
            }
            else
            {
                _timer.Stop();
                TimeBool = false;
                OnPropertyChanged(nameof(TimeBool));
            }
        }

        public bool TimeBool
        {
            get => _timeBool;
            set
            {
                _timeBool = value;
                InverseTimeLoop = !value;
                if (_timeBool == true)
                    Duration = (Duration)TimeSpan.FromSeconds(Convert.ToInt32(_settingsDal.GetSetting("TimeInterval")));
                Time();
                OnPropertyChanged(nameof(TimeBool));
                SaveToConfig(nameof(TimeBool), value);
            }
        }
        private int _timeInterval;
        public int TimeInterval
        {
            get => _timeInterval;
            set
            {
                _timeInterval = value; OnPropertyChanged(nameof(TimeInterval));
                Time();
                Duration = (Duration)TimeSpan.FromSeconds(value);
                SaveToConfig("TimeInterval", _timeInterval);
            }
        }


        #endregion

        #region Commands   
        public ICommand OpenPopupCommand { get; set; }
        public ICommand ClosePopupCommand { get; set; }

        private ICommand _updateManuallyCommand;
        public ICommand UpdateManuallyCommand
        {
            get => _updateManuallyCommand ?? (_updateManuallyCommand = new AsyncRelayCommand(SendDataToMaster));
            set
            {
                _updateManuallyCommand = value;
                OnPropertyChanged(nameof(CheckUpdateData2));
            }

        }

        private ICommand _checkUpdateDataGrid2Command;
        public ICommand CheckUpdateDataGrid2Command
        {
            get => _checkUpdateDataGrid2Command ?? (_checkUpdateDataGrid2Command = new AsyncRelayCommand(CheckUpdateData2));
            set
            {
                _checkUpdateDataGrid2Command = value;
                OnPropertyChanged(nameof(CheckUpdateData2));
            }
        }
        private ICommand _createTableCommand;

        public ICommand CreateTableCommand
        {
            get => _createTableCommand ?? (_createTableCommand = new AsyncRelayCommand(CreateTable));
            set
            {
                _createTableCommand = value;
                OnPropertyChanged(nameof(CreateTableCommand));
            }
        }

        private ICommand _savePathCommand;
        public ICommand SavePathCommand
        {
            get => _savePathCommand ?? (_savePathCommand = new AsyncRelayCommand(SaveLibraryPath));
            set
            {
                _savePathCommand = value;
                OnPropertyChanged(nameof(SavePathCommand));
            }
        }

        private ICommand _saveToolPathCommand;

        public ICommand SaveToolPathCommand
        {
            get => _saveToolPathCommand ?? (_saveToolPathCommand = new AsyncRelayCommand(SaveToolPath));
            set
            {
                _saveToolPathCommand = value;
                OnPropertyChanged(nameof(SaveToolPathCommand));
            }
        }

        private ICommand _saveToolDbPathCommand;

        public ICommand SaveToolDbPathCommand
        {
            get => _saveToolDbPathCommand ?? (_saveToolDbPathCommand = new AsyncRelayCommand(SaveToolDbPath));
            set
            {
                _saveToolDbPathCommand = value;
                OnPropertyChanged(nameof(SaveToolDbPathCommand));
            }
        }
        private ICommand _connectToSystemCommand;
        public ICommand ConnectToSystemCommand
        {
            get => _connectToSystemCommand ?? (_connectToSystemCommand = new RelayCommand(Connect));
            set
            {
                _connectToSystemCommand = value;
                OnPropertyChanged(nameof(ConnectToSystemCommand));
            }
        }
        public void Connect()
        {
            _settingsDal.UpdateSetting("HostName", HostName);
            _settingsDal.UpdateSetting("Port", Port);
            IoC.Communication.ServerAddress = _settingsDal.GetSetting("HostName").ToString();
            IoC.Communication.Port = Convert.ToInt32(_settingsDal.GetSetting("Port"));
            IoC.Communication.Connect();
            Connection = IoC.Communication.ConnectionStatus;
            //toolCenter.Connect();
            //    Connection = toolCenter._connected;
        }
        public ICommand OpenRfidWindowCommand { get; set; }
        public ICommand OpenSettingsCommand { get; set; }
        #endregion

        #region DataIO
        //RFID
        private void RfidReader()
        {
            RFIDView rfidView = new RFIDView();
        }

        private void OpenSettings()
        {
            Views.Settings settings = new Views.Settings();
           

        }
        private ObservableCollection<DbToolModel> _iLoaderToolList;
        public ObservableCollection<DbToolModel> ILoaderToolList
        {
            get => _iLoaderToolList;
            set
            {
                _iLoaderToolList = value;
                OnPropertyChanged(nameof(ILoaderToolList));
            }
        }
        // Declaration property for first data grid
        private ObservableCollection<MachineData> _dataGrid1Data;
        public ObservableCollection<MachineData> DataGrid1Data
        {
            get => _dataGrid1Data;
            set
            {
                _dataGrid1Data = value;
                OnPropertyChanged();
            }
        }
        

        private ObservableCollection<Tool> _dataGrid2Data;
        public ObservableCollection<Tool> DataGrid2Data
        {
            get => _dataGrid2Data;
            set
            {
                _dataGrid2Data = value;
                OnPropertyChanged();
            }
        }
        private void OpenPopup()
        {
            IsPopupOpen = true;
        }
        private async Task ClosePopup()
        {
            await CreateTable();
            IsPopupOpen = false;

        }
        private async Task SaveToConfig(string var, object value)
        {

            await _statusDal.UpdateStatus(var, value);

        }
        private async Task CheckUpdateData2()
        {
            return;
        }
        private async Task CreateTable()
        {
            //await _machineDataDal.CreateDT(CNCText);
            if (CNCText != "Library")
            {
                await _toolDataDal.CreateToolDT(CNCText);

            }

            MessageBox.Show($"{CNCText} Named Table \nCreated Succesfully!");
        }

        public void UpdateGrids_User()
        {

            List<MachineData> machineDataList = DataGrid1Data.Cast<MachineData>().ToList();
            List<Tool> toolDataList = DataGrid2Data.Cast<Tool>().ToList();

            // _machineDataDal.UpdateAll(machineDataList);


            _toolDataDal.UpdateAll(toolDataList).Wait();



             UpdateGrids();
        }
       
            // UpdateGrids method
            private void UpdateGrids()
            {
                // Ensure that this method is called on the UI thread
                App.Current.Dispatcher.Invoke(() =>
                {
                    List<MachineData> data = _machineDataDal.GetAllMachineData().Result;
                    List<Tool> toolData = _toolDataDal.GetAllTools().Result;

                    // Clear the current data in the data grid
                    DataGrid1Data.Clear();
                    DataGrid2Data.Clear();

                    // Add new data to the data grids
                    foreach (var machineData in data)
                    {
                        DataGrid1Data.Add(machineData);
                    }

                    foreach (var tool in toolData)
                    {
                        DataGrid2Data.Add(tool);
                    }
                });
            }


        
        public static MachineData FromCsv(string csvLine)
        {
            if (csvLine != String.Empty)
            {
                string[] values = csvLine.Split(',');
                MachineData machineData = new MachineData();
                List<string> variableList = new List<string>();
                machineData.Tool = values[0];
                machineData.IdNumber = Convert.ToInt32(values[1]);
                machineData.ToolType = values[2];
                machineData.ToolUserType = values[3];
                machineData.Permanent = Convert.ToInt32(values[4]);
                machineData.UnitsDiameter = values[5];
                machineData.Diameter = Convert.ToDecimal(values[6]);
                machineData.Angle = Convert.ToInt32(values[7]);
                machineData.Radius = Convert.ToDouble(values[8]);
                machineData.ProfileRadius = Convert.ToInt32(values[9]);
                machineData.UpperRadius = Convert.ToInt32(values[10]);
                machineData.NumTeeth = Convert.ToInt32(values[11]);
                machineData.Description = values[12];
                machineData.TaperAngle = Convert.ToInt32(values[13]);
                machineData.ShankDiameter = Convert.ToDecimal(values[14]);
                machineData.UnitsLength = values[15];
                machineData.Length = Convert.ToDecimal(values[16]);
                machineData.TotalLength = Convert.ToInt32(values[17]);
                machineData.ShoulderLength = Convert.ToInt32(values[18]);
                machineData.Startshoulderlength = Convert.ToInt32(values[19]);
                machineData.TipLength = Convert.ToInt32(values[20]);
                machineData.CuttingLength = Convert.ToDecimal(values[21]);
                machineData.HLength = Convert.ToInt32(values[22]);
                machineData.Material = values[23];
                machineData.UnitsFeedSpin = values[24];
                machineData.FType = (values[25]);
                machineData.FeedXY = Convert.ToDecimal(values[26]);
                machineData.FeedZ = Convert.ToDecimal(values[27]);
                machineData.FeedFinish = Convert.ToDecimal(values[28]);
                machineData.SType = (string)values[29];
                machineData.Spin = Convert.ToDecimal(values[30]);
                machineData.SpinFinish = Convert.ToDecimal(values[31]);
                machineData.FeedZPenetration = values[32];
                machineData.FeedLeadIn = Convert.ToInt32(values[33]);
                machineData.FeedLeadOut = Convert.ToInt32(values[34]);
                machineData.FeedLink = Convert.ToInt32(values[35]);
                machineData.ToolName = values[36];
                machineData.ToolGroup = values[37];
                machineData.HolderName = values[38];
                machineData.GroupHolderName = values[39];
                machineData.Direction = values[40];
                machineData.ChamferLength = Convert.ToInt32(values[41]);
                machineData.TipDiameter = Convert.ToDecimal(values[42]);
                machineData.ShoulderDiameter = Convert.ToDecimal(values[43]);
                machineData.ShoulderAngle = Convert.ToDecimal(values[44]);
                machineData.Message1 = values[45];
                machineData.Message2 = values[46];
                machineData.Message3 = values[47];
                machineData.Message4 = values[48];
                machineData.Message5 = values[49];
                machineData.FloodCoolant = Convert.ToInt32(values[50]);
                machineData.MistCoolant = Convert.ToInt32(values[51]);
                machineData.HighPressureCoolant = Convert.ToInt32(values[52]);
                machineData.LowPressureCoolant = Convert.ToInt32(values[53]);
                machineData.ThroughHighPressureCoolant = Convert.ToInt32(values[54]);
                machineData.ThroughLowPressureCoolant = Convert.ToInt32(values[55]);
                machineData.AirBlastCoolant = Convert.ToInt32(values[56]);
                machineData.MinimumQuantityLubricationCoolant = Convert.ToInt32(values[57]);
                machineData.MinimumQuantityLubricationValue = Convert.ToInt32(values[58]);
                machineData.Rough = Convert.ToInt32(values[59]);
                machineData.SpinDirection = values[60];
                machineData.SpinLimit = Convert.ToInt32(values[61]);
                machineData.ThreadingStandard = values[62];
                machineData.ThreadStandardByUser = values[63];
                machineData.ThreadingTableStandard = values[64];
                machineData.NumThreads = Convert.ToInt32(values[65]);
                machineData.Pitch = Convert.ToDecimal(values[66]);
                machineData.PitchUnit = values[67];

                machineData.CNCName = _cncName = "";

                return machineData;
            }
            return null;
        }

        public List<MachineData> MachineDataCsvToList(string path)
        {
            return File.ReadAllLines(path)
                    .Where(v => !string.IsNullOrWhiteSpace(v))
                    .Skip(1)
                    .Select(FromCsv)
                    .ToList();
        }
        public List<Tool> ToolCsvToList(string path)
        {
            return File.ReadAllLines(path)
                    .Where(v => !string.IsNullOrWhiteSpace(v))
                    .Skip(1)
                    .Select(ToolDataFromCsv)
                    .ToList();
        }
        public Tool ToolDataFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(';');
            Tool tool = new Tool();

            tool.Id = Convert.ToInt32(values[0]);
            tool.Number = values[1];
            tool.Name = values[2];
            tool.Radius = Convert.ToDouble(values[3]);
            tool.Diameter = Convert.ToDouble(values[4]);
            tool.CoordinateZ = Convert.ToDouble(values[5]);
            tool.TheoreticalRadius = Convert.ToDouble(values[6]);
            tool.TheoreticalZ = Convert.ToDouble(values[7]);
            tool.LifeOf = values[8];
            tool.CutterEdgeRadius = Convert.ToDouble(values[9]);
            tool.CutterEdgeOrient = Convert.ToInt32(values[10]);
            tool.Type = Convert.ToInt32(values[11]);
            tool.CNCName = _cncName = "";

            return tool;
        }
        private void SaveToolDbPath()
        {

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = (String.IsNullOrEmpty(_statusDal.GetStatus("ToolDbPath").ToString()) ? Environment.CurrentDirectory : System.IO.Path.GetDirectoryName(_statusDal.GetStatus("ToolDbPath").ToString().Replace("\\", "//"))) ?? string.Empty;

            openFileDialog.RestoreDirectory = true;
            if (openFileDialog.ShowDialog() == true)
            {

                _statusDal.UpdateStatus(nameof(ToolDbPath), openFileDialog.FileName);
                // _toolDataDal.DeleteAll();
                try
                {
                    //_toolDataDal.AddAll(ToolCsvToList(_statusDal.GetStatus("ToolPath").ToString()), cncName: _cncText.Split('.')[0]).Wait();
                }
                catch
                {
                    MessageBox.Show(msgFile);
                    SaveToolDbPath();
                }
                _machineDataDal.UpdateAll(_toolDataDal.GetAllTools().Result);
                UpdateGrids();


            }
        }
        public async Task SaveLibraryPath()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "CSV Files (*.csv)|*.csv|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = (String.IsNullOrEmpty(_statusDal.GetStatus("LibraryPath").ToString()) ? Environment.CurrentDirectory : System.IO.Path.GetDirectoryName(_statusDal.GetStatus("LibraryPath").ToString().Replace("\\", "//"))) ?? string.Empty;

            openFileDialog.RestoreDirectory = true;
            if (openFileDialog.ShowDialog() == true)
            {
                _LibraryPath = openFileDialog.FileName;
                await _statusDal.UpdateStatus(nameof(LibraryPath), _LibraryPath);
                _cncText = Path.GetFileName(_LibraryPath);
                _cncName = _cncText;
                await SaveLibraryPathAuto(_LibraryPath);

                 UpdateGrids();
            }
        }

        private async Task SaveLibraryPathAuto(string path)
        {

            MachineDataDal machineDataDal = new MachineDataDal();

            // await _machineDataDal.DeleteAll();
            try
            {
                await _machineDataDal.DeleteAll();
                await _machineDataDal.AddAll(MachineDataCsvToList(_statusDal.GetStatus("LibraryPath").ToString()), cncName: _cncText.Split('.')[0]);
            }
            catch
            {
                MessageBox.Show(msgFile);
                await SaveLibraryPath();
            }

        }

        public string SaveAllToolsPath()
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                _filesPath = folderBrowserDialog.SelectedPath;
                return _filesPath;
            }
            else
            {
                return  "";
            }
        }   

        private List<string> _cncFiles = new List<string>();

        public async Task SaveToolPath()
        {
            var p = SaveAllToolsPath();
            if (p != "")
            {
                await _statusDal.UpdateStatus(nameof(ToolPath), p);
            }
            

            try
            {
                await UpdateToolsLoopFunc();
            }
            catch
            {
                MessageBox.Show(msgFile);
                await SaveToolPath();
            }


            _cncText = Path.GetFileName(_path);

            _cncName = _cncText;


            await _machineDataDal.UpdateAll(_toolDataDal.GetAllTools().Result);
             UpdateGrids();


        }

        private async Task UpdateToolsLoopFunc()
        {
            _cncFiles.Clear();
            await _toolDataDal.DeleteAll();
            foreach (var cncFile in Directory.GetFiles(_filesPath))
            {
                var name = Path.GetFileName(cncFile).Split('.')[0];
                if (!IsReadableToolData(cncFile))
                {
                    continue;
                }

                try
                {
                    _cncFiles.Add(name);
                    await _toolDataDal.CreateToolDT(name);



                }
                catch
                {
                    // ignored
                }
                finally
                {
                    await _toolDataDal.AddAll(ToolCsvToList(cncFile), cncName: name);
                }
            }
        }

        private async Task SendDataToMaster()
        {
            Task.Run(() =>
            {
                IoC.Communication.SendData();

                Connection = IoC.Communication.ConnectionStatus;
                ILoaderToolList = IoC.Communication.GetILoaderToolList();
            }).Wait(1000);
            
        }

        private void ReadFromFile_Continuous()
        {
            // await _machineDataDal.DeleteAll();
            // await _toolDataDal.DeleteAll();
            _cncText = Path.GetFileName(_path);

             UpdateToolsLoopFunc().Wait();

            //await _machineDataDal.AddAll(MachineDataCsvToList(_statusDal.GetStatus("LibraryPath").ToString()), cncName: _cncText.Split('.')[0]);
            //await _toolDataDal.AddAll(ToolCsvToList(_statusDal.GetStatus("ToolPath").ToString()), cncName: _cncText.Split('.')[0]);


             _machineDataDal.UpdateAll(_toolDataDal.GetAllTools().Result).Wait();

             UpdateGrids();
            UpdateGrids_User();

            SendDataToMaster();
        }

        private bool IsReadableToolData(string path)
        {

            if (File.ReadLines(path).First() == $"Id;Number;Name;Radius;Diameter;Z;Theorical Radius;Theorical Z;Life of;Cutter edge radius;Cutter edge orient.;Type"
                || File.ReadLines(path).First() == $"Id;NUMARA;ISIM;RADYUS;CAP;Z;TEORIK RADYUS;TEORIK Z;HAYATI;KESICI UC RADYUSU;KESICI UC YONU;TIP")
            {
                return true;

            }
            else
            {
                return false;

            }
        } 
        #endregion



    }
}
