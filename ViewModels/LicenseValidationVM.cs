﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolDataLibraryAsync.ViewModels
{
    public class LicenseValidationVM
    {
        private const string AllowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        public string GenerateLicenseKey(int length)
        {
            Random random = new Random();
            string licenseKey = new string(Enumerable.Repeat(AllowedChars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());

            return licenseKey;
        }

        public bool ValidateLicenseKey(string licenseKey)
        {
            // Add your validation logic here
            // Example: Check license key format, expiration date, etc.

            // For demonstration purposes, we'll assume any license key of length 16 is valid
            return licenseKey.Length == 16;
        }
        public LicenseValidationVM()
        {
            LicenseValidationVM keyGenerator = new LicenseValidationVM();
            string licenseKey = keyGenerator.GenerateLicenseKey(16);

            bool isValid = keyGenerator.ValidateLicenseKey(licenseKey);
        }
    }
}
