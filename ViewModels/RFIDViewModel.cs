﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Ports;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using ToolDataLibraryAsync.Commands;
using ToolDataLibraryAsync.Data_Access.SQLite;
using ToolDataLibraryAsync.Data_Access.SQLite.SettingsDb;
using ToolDataLibraryAsync.Models;
using ToolDataLibraryAsync.RabbitMQ;
using MessageBox = System.Windows.MessageBox;

namespace ToolDataLibraryAsync.ViewModels
{
    public class RFIDViewModel :ViewModelBase
    {
        private static RFIDViewModel instance;
        public static RFIDViewModel Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new RFIDViewModel();
                }
                return instance;
            }
            set { instance = value;
                    }
        }
        private SettingsDal _settingsDal;
        #region Variables
        private DbToolModel _dbToolModel;
        public DbToolModel DbToolModel
        {
            get => _dbToolModel;
            set
            {
                _dbToolModel = value;
                
                OnPropertyChanged(nameof(DbToolModel));
            }
        }
        private bool _scanButtonFlag;

        public bool ScanButtonFlag
        {
            get => _scanButtonFlag;
            set
            {
                _scanButtonFlag = value;
                OnPropertyChanged(nameof(ScanButtonFlag));
            }
        }
        private bool _addressFlag = true;

        public bool AddressFlag
        {
            get => _addressFlag;
            set
            {
                _addressFlag=value;
                OnPropertyChanged(nameof(AddressFlag));
            }
        }
        private bool _lengthFlag = true;

        public bool LengthFlag
        {
            get => _lengthFlag;
            set
            {
                _lengthFlag = value;
                OnPropertyChanged(nameof(LengthFlag));
            }
        }

        private bool _dataToBeSentFlag = false;
        public bool DataToBeSentFlag
        {
            get => _dataToBeSentFlag;
            set
            {
                _dataToBeSentFlag = value;
                OnPropertyChanged(nameof(DataToBeSentFlag));
            }
        }
        #endregion
        public event EventHandler<string> DataRecieved = null;

        public Dictionary<char, string> ErrorCode = new Dictionary<char, string>()
        {
            {'1', "No data carrier present."},
            {'2', "Read error."},
            {'3', "Read canceled because the data carrier was removed."},
            {'4', "Write error."},
            {'5', "Write canceled because the data carrier was removed."},
            {'6', "Parity or Stop Bit error"},
            //{'7', "Telegram format error."},
            {'8', "BCC error. The transmitted BCC is wrong.."},
            {'9', "TCable break to selected read/write head or read/write head not connected. LED CT Present/Operating flashes."},
            {'D', "Communication fault with the read/write head."},
            {'E', " CRC error The CRC checksum on the data carrier is incorrect."},
            {'F', "Addressing error"},
            {'G', "Job not supported by the data carrier"},
            {'I', "I EEPROM error"},
        };
        private bool _isMQConnected;
        public bool IsMQConnected { get => _isMQConnected; set { _isMQConnected = value; OnPropertyChanged(nameof(IsMQConnected)); } }
        private bool _isConnected;
        public bool IsConnected { get => _isConnected; set { _isConnected = value; OnPropertyChanged(nameof(IsConnected)); } }

        SerialPort _serialPort = new SerialPort();
        public enum EnumRS232Mode{
        None,
        Read,
        Write
        };
        public EnumRS232Mode Mode { get; set; } = EnumRS232Mode.Read;
        
        private string _dataResponseString;
        public string DataResponseString { 
            get => _dataResponseString;
            set {
                _dataResponseString = value;
                OnPropertyChanged(nameof(DataResponseString));
                } }
        private string _dataAddress="0000";

        public string DataAddress
        {
            get => _dataAddress;
            set
            { _dataAddress = value; 
                OnPropertyChanged(nameof(DataAddress));
                if (_dataAddress.Length <= 0)
                {
                    _dataAddress = "0000";
                }
            }
        }
        private string _dataLength="0000";
        public string DataLength { get => _dataLength;
            set { _dataLength = value;
                OnPropertyChanged(nameof(DataLength));
                if (_dataLength.Length <= 0 || Convert.ToInt16(_dataLength) > 8)
                {
                    _dataLength = "0000";
                }
                
            }

        }
        private string dataString;
        public string DataCommandString { get; set; }
        private string _dataToBeSentString="";
        public string DataToBeSentString { get => _dataToBeSentString;
            set { _dataToBeSentString = value; OnPropertyChanged(nameof(DataToBeSentString)); } }
        public List<byte> RecievedData { get; set; }
        public byte[] DataToBeSent { get; set; }

        public RFIDViewModel()
        {
            _settingsDal = new SettingsDal();
            DbToolModel = new DbToolModel();
            _serialPort.DataReceived += new SerialDataReceivedEventHandler(DataRecievedHandler);
            //_serialPort.DataReceived += (sender, e) =>
            //{
            //    SerialPort sp = (SerialPort)sender;
            //    var indata = sp.ReadExisting();
            //    var dataBytes = Encoding.ASCII.GetBytes(indata);

            //    //error search!
            //    if (Array.Exists(dataBytes, p => p == 21)) //#NAK
            //    {
            //        var index = 0;
            //        var errorString = "";
            //        if (Array.FindAll(dataBytes, d => d == 21).Length == 1)
            //        {
            //            try
            //            {
            //                index = Array.FindIndex(dataBytes, p => p == 21);
            //                errorString = ErrorCode[Convert.ToChar(dataBytes[index + 1])];
            //            }
            //            catch
            //            {

            //                Listen();
            //            }
            //        }

            //        DataResponseString += errorString + Environment.NewLine;
            //        Mode = EnumRS232Mode.None;
            //    }


            //    if (Mode == EnumRS232Mode.Read)
            //    {
            //        if (dataBytes?.Length == 2 &&
            //            dataBytes?[0] == 6 && //#ACK
            //            dataBytes?[1] == 48) //0
            //        {

            //            foreach (var item in dataBytes)
            //                WriteDataToScreen(item);
            //            DataResponseString += Environment.NewLine;

            //            var data = new byte[] { 2 }; //#STX
            //            DataCommandString += " #STX " + Environment.NewLine;
            //            _serialPort.Write(data, 0, data.Length);
            //        }
            //        else
            //        {
            //            for (var i = 0; i < dataBytes.Length; i++)
            //            {
            //                if (dataBytes[i] != 13)
            //                    RecivedData.Add(dataBytes[i]);
            //                else
            //                {
            //                    foreach (var data in RecivedData)
            //                        WriteDataToScreen(data);
            //                    OnDataRecieve(DataResponseString);
            //                    DataResponseString += " #CR " + Environment.NewLine;
            //                    Mode = EnumRS232Mode.None;
            //                }
            //            }

            //        }
            //    }
            //    else if (Mode == EnumRS232Mode.Write)
            //    {
            //        if (dataBytes?.Length == 2 &&
            //            dataBytes?[0] == 6 && //#ACK
            //            dataBytes?[1] == 48) //0
            //        {
            //            foreach (var data in dataBytes)
            //                WriteDataToScreen(data);
            //            DataResponseString += Environment.NewLine;

            //            var sendDataLenght = DataToBeSent.Length + 2;
            //            var sendDataBytes = new byte[sendDataLenght];

            //            sendDataBytes[0] = 2; //#STX
            //            sendDataBytes[sendDataLenght - 1] = 13; //#CR
            //            DataCommandString += " #STX ";
            //            for (int i = 1; i <= DataToBeSent.Length; i++)
            //            {
            //                sendDataBytes[i] = DataToBeSent[i - 1];
            //                DataCommandString += Convert.ToChar(sendDataBytes[i]);
            //            }

            //            OnDataRecieve(DataResponseString);
            //            DataCommandString += " #CR ";
            //            DataCommandString += Environment.NewLine;
            //            _serialPort.Write(sendDataBytes, 0, sendDataBytes.Length);

            //            Mode = EnumRS232Mode.None;

            //        }
            //    }
            //    else
            //    {
            //        foreach (var data in dataBytes)
            //            WriteDataToScreen(data);
            //        DataResponseString += Environment.NewLine;
            //    }
            //};
            ConnectionOpen();
            
            MQPort = Convert.ToInt32(_settingsDal.GetSetting("MQPort"));
            ConnectToMQ();
            //IoC.RabbitMQClient.CreateConn(MQPort);
            IsMQConnected = IoC.RabbitMQClient.mqConnection;
           



            ReadWrite = new ObservableCollection<string>()
            {
               "Read","Write"
            };
            //IoC.RabbitMQClient.SendDataToMQ<DbToolModel>("Get", 1);
            //DbToolModel = IoC.RabbitMQClient.ResponseModel as DbToolModel;
        }


        private void DataRecievedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            {
                SerialPort sp = (SerialPort)sender;
                var indata = sp.ReadExisting();
                var dataBytes = Encoding.ASCII.GetBytes(indata);

                //error search!
                if (Array.Exists(dataBytes, p => p == 21)) //#NAK
                {
                    var index = 0;
                    var errorString = "";
                    if (Array.FindAll(dataBytes, d => d == 21).Length == 1)
                    {
                        index = Array.FindIndex(dataBytes, p => p == 21);
                        errorString = ErrorCode[Convert.ToChar(dataBytes[index + 1])];
                    }
                    DataResponseString += errorString + Environment.NewLine;
                    //Mode = EnumRS232Mode.None;
                }


                if (Mode == EnumRS232Mode.Read)
                {
                    if (dataBytes?.Length == 2 &&
                        dataBytes?[0] == 6 && //#ACK
                        dataBytes?[1] == 48) //0
                    {

                        var data = new byte[] { 2 }; //#STX
                        DataCommandString += " #STX " + Environment.NewLine;
                        _serialPort.Write(data, 0, data.Length);
                    }
                    else
                    {
                        for (var i = 0; i < dataBytes.Length; i++)
                        {
                            if (dataBytes[i] != 13)
                                RecievedData.Add(dataBytes[i]);
                            else
                            {
                                foreach (var data in RecievedData)
                                    WriteDataToScreen(data);
                                //IoC.RabbitMQClient.SendDataToMQ<DbToolModel>("Get");
                                OnDataRecieve(DataResponseString);
                                
                               // Mode = EnumRS232Mode.None;
                            }
                        }
                        try
                        {
                            var ID = Convert.ToInt32(DataResponseString.Substring(DataResponseString.Length - 4));
                            IoC.Communication.SendIdFromRFID(ID);
                            Thread.Sleep(250);
                            //Instance.DbToolModel = IoC.Communication.retrievedTool;

                            //if (IoC.RabbitMQClient.mqConnection == true)
                            //    IoC.RabbitMQClient.SendDataToMQ<DbToolModel>("Get", ID);


                            FillInfo().Wait();


                        }
                        catch
                        {

                        }

                    }

            
                }
                else if (Mode == EnumRS232Mode.Write)
                {
                    if (dataBytes?.Length == 2 &&
                        dataBytes?[0] == 6 && //#ACK
                        dataBytes?[1] == 48) //0
                    {
                        foreach (var data in dataBytes)
                            WriteDataToScreen(data);
                        DataResponseString += Environment.NewLine;

                        var sendDataLenght = DataToBeSent.Length + 2;
                        var sendDataBytes = new byte[sendDataLenght];

                        sendDataBytes[0] = 2; //#STX
                        sendDataBytes[sendDataLenght - 1] = 13; //#CR
                        DataCommandString += " #STX ";
                        for (int i = 1; i <= DataToBeSent.Length; i++)
                        {
                            sendDataBytes[i] = DataToBeSent[i - 1];
                            DataCommandString += Convert.ToChar(sendDataBytes[i]);
                        }

                        OnDataRecieve(DataResponseString);
                        DataCommandString += " #CR ";
                        DataCommandString += Environment.NewLine;
                        _serialPort.Write(sendDataBytes, 0, sendDataBytes.Length);

                        //Mode = EnumRS232Mode.None;

                    }
                }
                else
                {
                    foreach (var data in dataBytes)
                        WriteDataToScreen(data);
                    DataResponseString += Environment.NewLine;
                }
            };
            

        }
        private async Task FillInfo()
        {
            await Task.Run(() =>
            {
                //Thread.Sleep(2500);
                //DbToolModel = IoC.RabbitMQClient.ResponseModel as DbToolModel;
                
                //FunctionId = DbToolModel.FunctionId;
                //Description = DbToolModel.Description;
            });
            
        }
        private int _mQPort;
        public int MQPort
        {
            get => _mQPort;
            set
            {
                _mQPort = value;
                OnPropertyChanged(nameof(MQPort));
            }
        }
        private string _rfidCMD = "Read";
        public string RFIDCMD
        {
            get => _rfidCMD;
            set
            {
                _rfidCMD = value;
                OnPropertyChanged(nameof(RFIDCMD));
                if (_rfidCMD == "Read")
                {
                    Mode = EnumRS232Mode.Read;
                    AddressFlag = true;
                    LengthFlag = true;
                    DataToBeSentFlag = false;
                }
                else if (_rfidCMD == "Write")
                {
                    Mode = EnumRS232Mode.Write;
                    AddressFlag = true;
                    LengthFlag = true;
                    DataToBeSentFlag =true;
                }
                else
                    Mode = EnumRS232Mode.None;

            }
        }
        private ObservableCollection<string> _readWrite;

        public ObservableCollection<string> ReadWrite
        {
            get => _readWrite;
            set
            {
                _readWrite = value;
                OnPropertyChanged(nameof(ReadWrite));

            }
        }
        private ICommand _connectToMQCommand;
        public ICommand ConnectToMQCommand
        {
            get => _connectToMQCommand ?? (_connectToMQCommand = new RelayCommand(ConnectToMQ));
            set
            {
                _connectToMQCommand = value;
                OnPropertyChanged(nameof(_connectToMQCommand));
            }
        }

        private void ConnectToMQ()
        {
            _settingsDal.UpdateSetting("MQPort", MQPort);
            IoC.RabbitMQClient.CreateConn(MQPort);
            IsMQConnected = IoC.RabbitMQClient.mqConnection;
        }

        private ICommand _connectToRS232Command;
        public ICommand ConnectToRS232Command
        {
            get => _connectToRS232Command ?? (_connectToRS232Command = new RelayCommand(Connect));
            set
            {
                _connectToRS232Command = value;
                OnPropertyChanged(nameof(ConnectToRS232Command));
            }
        }
        private ICommand _rfidScanCommand;

        public ICommand RFIDScanCommand
        {
            get => _rfidScanCommand ?? (_rfidScanCommand = new RelayCommand(Listen));
            set
            {
                _rfidScanCommand = value;
                OnPropertyChanged(nameof(_rfidScanCommand));
            }
        }

        public void Connect()

        {
            _serialPort.Close();
            if (!_serialPort.IsOpen)
            {

                _serialPort.PortName = Settings.Instance.SelectedPort;
                _serialPort.BaudRate = 9600;
                _serialPort.DataBits = 8;
                _serialPort.StopBits = StopBits.One;
                _serialPort.Handshake = Handshake.None;
                _serialPort.Parity = Parity.Even;


                try
                {
                    _serialPort.Open();
                    if(_serialPort.IsOpen)
                    {
                        IsConnected = true;
                        OnPropertyChanged(nameof(IsConnected));
                    }
                    
                }
                catch { IsConnected = false; OnPropertyChanged(nameof(IsConnected)); }
                
                OnPropertyChanged(nameof(IsConnected));
               
            }
            else
            {
                IsConnected = true;
                OnPropertyChanged(nameof(IsConnected));
            }
           
        }

        public bool ConnectionOpen()
        {
            if (!_serialPort.IsOpen)
            {

                _serialPort.PortName = Settings.Instance.SelectedPort;
                _serialPort.BaudRate = 9600;
                _serialPort.DataBits = 8;
                _serialPort.StopBits = StopBits.One;
                _serialPort.Handshake = Handshake.None;
                _serialPort.Parity = Parity.Even;


                try
                {
                    _serialPort.Open();
                    IsConnected = true; OnPropertyChanged(nameof(IsConnected));
                }
                catch { IsConnected = false; OnPropertyChanged(nameof(IsConnected));  return false;  }
                
                OnPropertyChanged(nameof(IsConnected));
                return true;
            }
            else
            {
                IsConnected = true; OnPropertyChanged(nameof(IsConnected));
            }
            return false;
        }
        public void ConnectionClose()
        {
            if (_serialPort.IsOpen)
            {
                _serialPort.Close();
                IsConnected = false;
                OnPropertyChanged(nameof(IsConnected));
            }
        }

        public void Listen()
        {
            if (!_serialPort.IsOpen)
            {
                IsConnected = false;
                
                OnPropertyChanged(nameof(IsConnected));
                return;
            }

            if (Mode  == EnumRS232Mode.Write)
            {
                dataString = "W" + DataAddress + DataLength;
            }
            if(Mode == EnumRS232Mode.Read)
            {
                dataString = "R" + DataAddress + DataLength;
            }

            var pattern = "[RW][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]";
            var ragex = new Regex(pattern);

            var match = ragex.Match(dataString);
            if (match.Success)
            {
                if (dataString.FirstOrDefault() == 'R')
                {
                    var startAdress = Convert.ToInt32(dataString.Substring(1, 4));
                    var dataLenght = Convert.ToInt32(dataString.Substring(5, 4));
                    if (startAdress >= 0 && startAdress < 2000 &&   //start adress 0-1999 allowed
                        dataLenght > 0 && dataLenght <= 2000)       //data lenght 1-2000 allowed
                    {
                        var dataByte = Encoding.ASCII.GetBytes(dataString);
                        dataByte = dataByte.Concat(new byte[] { 13 }).ToArray(); // adds #CR to end.

                        Mode = EnumRS232Mode.Read;
                        RecievedData = new List<byte>();
                        DataCommandString = string.Empty;
                        DataResponseString = string.Empty;
                        DataCommandString += dataString + " #CR " + Environment.NewLine;
                        try
                        {
                            _serialPort.Write(dataByte, 0, dataByte.Length);

                        }
                        catch
                        {
                            IsConnected = false;
                            OnPropertyChanged(nameof(IsConnected));
                            return;
                        }
                    }
                    else
                    {
                        //datalenght or startadress errorK
                        Mode = EnumRS232Mode.None;
                        return;
                    }
                }
                else if (dataString.FirstOrDefault() == 'W')
                {
                    var startAdress = Convert.ToInt32(dataString.Substring(1, 4));
                    var dataLenght = Convert.ToInt32(dataString.Substring(5, 4));
                    if (startAdress >= 0 && startAdress < 2000 &&   //start adress 0-1999 allowed
                        dataLenght > 0 && dataLenght <= 2000)       //data lenght 1-2000 allowed
                    {
                        var dataByte = Encoding.ASCII.GetBytes(dataString);
                        dataByte = dataByte.Concat(new byte[] { 13 }).ToArray(); // adds #CR to end.

                        if (DataToBeSentString.Length == dataLenght)
                        {
                            Mode = EnumRS232Mode.Write;
                            DataToBeSent = Encoding.ASCII.GetBytes(DataToBeSentString);
                            DataCommandString = string.Empty;
                            DataResponseString = string.Empty;
                            DataCommandString += dataString + " #CR " + Environment.NewLine;
                            _serialPort.Write(dataByte, 0, dataByte.Length);
                        }
                        else
                        {
                            //datalenght not equal sent data error
                            Mode = EnumRS232Mode.None;
                            return;
                        }
                    }
                    else
                    {
                        //datalenght or startadress error
                        Mode = EnumRS232Mode.None;
                        return;
                    }
                }
            }
            else
            {
                //error with wrong pattern
                return;
            }
        }

        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {

            SerialPort sp = (SerialPort)sender;
            var indata = sp.ReadExisting();
            var dataBytes = Encoding.ASCII.GetBytes(indata);
            
            //error search!
            if (Array.Exists(dataBytes, p => p == 21))  //#NAK
            {
                var index = 0;
                var errorString = "";
                if (Array.FindAll(dataBytes, d => d == 21).Length == 1)
                {
                    try
                    {
                        index = Array.FindIndex(dataBytes, p => p == 21);
                        errorString = ErrorCode[Convert.ToChar(dataBytes[index + 1])];
                    }
                    catch
                    {

                        Listen();
                    }
                }
                DataResponseString += errorString + Environment.NewLine;
                Mode = EnumRS232Mode.None;
            }
            

            if (Mode == EnumRS232Mode.Read)
            {
                if (dataBytes?.Length == 2 &&
                    dataBytes?[0] == 6 &&   //#ACK
                    dataBytes?[1] == 48)  //0
                {

                    foreach (var item in dataBytes)
                        WriteDataToScreen(item);
                    DataResponseString += Environment.NewLine;

                    var data = new byte[] { 2 }; //#STX
                    DataCommandString += " #STX " + Environment.NewLine;
                    _serialPort.Write(data, 0, data.Length);
                }
                else
                {
                    for (var i = 0; i < dataBytes.Length; i++)
                    {
                        if (dataBytes[i] != 13)
                            RecievedData.Add(dataBytes[i]);
                        else
                        {
                            foreach (var data in RecievedData)
                                WriteDataToScreen(data);
                            OnDataRecieve(DataResponseString);
                            DataResponseString += " #CR " + Environment.NewLine;
                            Mode = EnumRS232Mode.None;
                        }
                    }

                }
            }
            else if (Mode == EnumRS232Mode.Write)
            {
                if (dataBytes?.Length == 2 &&
                    dataBytes?[0] == 6 &&   //#ACK
                    dataBytes?[1] == 48)  //0
                {
                    foreach (var data in dataBytes)
                        WriteDataToScreen(data);
                    DataResponseString += Environment.NewLine;

                    var sendDataLenght = DataToBeSent.Length + 2;
                    var sendDataBytes = new byte[sendDataLenght];

                    sendDataBytes[0] = 2;                       //#STX
                    sendDataBytes[sendDataLenght - 1] = 13;     //#CR
                    DataCommandString += " #STX ";
                    for (int i = 1; i <= DataToBeSent.Length; i++)
                    {
                        sendDataBytes[i] = DataToBeSent[i - 1];
                        DataCommandString += Convert.ToChar(sendDataBytes[i]);
                    }
                    OnDataRecieve(DataResponseString);
                    DataCommandString += " #CR ";
                    DataCommandString += Environment.NewLine;
                    _serialPort.Write(sendDataBytes, 0, sendDataBytes.Length);

                    Mode = EnumRS232Mode.None;

                }
            }
            else
            {
                foreach (var data in dataBytes)
                    WriteDataToScreen(data);
                DataResponseString += Environment.NewLine;
            }
            
        }

        private void WriteDataToScreen(byte data)
        {
            switch (data)
            {
                case 0:
                    DataResponseString += " #NUL ";
                    break;
                case 2:
                    DataResponseString += " #STX ";
                    break;
                case 6:
                    DataResponseString += " #ACK ";
                    break;
                case 13:
                    DataResponseString += " #CR ";
                    break;
                case 21:
                    DataResponseString += " #NAK ";
                    break;
                default:
                    DataResponseString += Convert.ToChar(data);
                    break;
            }
        }

        private void OnDataRecieve(string data)
        {
            DataRecieved?.Invoke(this, data);
        }
    }


}


