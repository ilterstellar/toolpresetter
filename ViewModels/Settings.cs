﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO.Ports;
using ToolDataLibraryAsync.Converters;
using ToolDataLibraryAsync.Data_Access.SQLite.SettingsDb;
using ToolDataLibraryAsync.Models;
using ToolDataLibraryAsync.Services;


namespace ToolDataLibraryAsync.ViewModels
{
    public class Settings : ViewModelBase
    {
        private static Settings _instance;
        private ObservableCollection<int> _fontSizes;
        private string[] _ports;
        private ObservableCollection<string> _styles;
        private ObservableCollection<int> _timeCollection;
        private readonly SettingsDal _settingsDal;
        private bool _timeFlag;
        public bool TimeFlag
        {
            
            set
            {
                _timeFlag = value;
                OnPropertyChanged(nameof(TimeFlag));
            }
            get
            {
                return _timeFlag;
            }
        }        


        public string[] Ports
        {
            set
            {
                _ports = value;
                OnPropertyChanged(nameof(Ports));
            }
            get
            { return _ports; }
        }
        public string SelectedPort
        {
            get => SettingService.Instance.SelectedPort;
            set
            {
                SettingService.Instance.SelectedPort = value;
                OnPropertyChanged(nameof(SelectedPort));
                _settingsDal.UpdateSetting("SerialPort", SelectedPort);
            }
        }
        public ObservableCollection<int> FontSizes
        {
            set
            {
                _fontSizes = value;
                OnPropertyChanged(nameof(FontSizes));
            }
            get
            { return _fontSizes; }
        }
        public ObservableCollection<string> Styles
        {
            set
            {
                _styles = value;
                OnPropertyChanged(nameof(Styles));
            }
            get { return _styles; }
        }

        public ObservableCollection<int> TimeCollection
        {
            get { return _timeCollection; }
            set
            {
                _timeCollection= value;
                OnPropertyChanged(nameof(TimeCollection));
            }
        }
        public static Settings Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Settings();
                }
                return _instance;
            }
        }
        private string _style;
        public string Style
        {
            get { return _style; }
            set
            {
                _style = value;
                OnPropertyChanged(nameof(Style));
                _settingsDal.UpdateSetting("Style", _style);
            }
        }
        public int SelectedFontSize
        {
            get => SettingService.Instance.SelectedFontSize;
            set
            {
                SettingService.Instance.SelectedFontSize = value;
                OnPropertyChanged(nameof(SelectedFontSize));
                
            }
        }
        public string SelectedLanguage
        {
            get => SettingService.Instance.SelectedLanguage;
            set
            {
                SettingService.Instance.SelectedLanguage = value;
                OnPropertyChanged(nameof(SelectedLanguage));
            }
        }
        public int SelectedTimeInterval
        {
            get => SettingService.Instance.SelectedTimeInterval;
            set
            {
                SettingService.Instance.SelectedTimeInterval = value;
                OnPropertyChanged(nameof(SelectedTimeInterval));
                _settingsDal.UpdateSetting("TimeInterval",SelectedTimeInterval);
            }
        }
        public string SelectedStyle
        {
            get=>SettingService.Instance.SelectedStyle;
            set
            {
                SettingService.Instance.SelectedStyle = value;
                OnPropertyChanged(nameof(SelectedStyle));
                Style = SelectedStyle;
                StyleChanger(Style);
            }
        }
        public void StyleChanger(string Style)
        {
            if (Style == "White")
            {
                ChangeStyle.ChangeTheme(new Uri("Styles/WhiteResourceDictionary.xaml", UriKind.Relative));
            }
            if (Style == "Violet")
            {
                ChangeStyle.ChangeTheme(new Uri("Styles/VioletResourceDictionary.xaml", UriKind.Relative));
            }
            if (Style == "Blue")
            {
                ChangeStyle.ChangeTheme(new Uri("Styles/BlueResourceDictionary.xaml", UriKind.Relative));
            }
            if (Style == "Green")
            {
                ChangeStyle.ChangeTheme(new Uri("Styles/GreenResourceDictionary.xaml", UriKind.Relative));
            }
        }
        public Settings()
        {
            _fontSizes = new ObservableCollection<int>();
            for (int i = 8; i <= 20; i += 2)
            {
                _fontSizes.Add(i);
            }

            _ports = SerialPort.GetPortNames();

            _styles = new ObservableCollection<string>()
            {
                "White","Blue","Violet","Green"
            };
            
            Languages = new ObservableCollection<Language>()
            {
                new Language { Content = cbxEnglish, Tag = "en-US" }, 
                new Language { Content = cbxTurkish, Tag = "tr-TR" }
            };
            TimeCollection = new ObservableCollection<int>()
            {
                5,10,15,20,25,30,60,120,180,240
            };
            _settingsDal = new SettingsDal();
            _style = _settingsDal.GetSetting("Style").ToString();
        }

        private CultureInfo _currentCulture;
        //private void Language()
        //{
        //    switch (Convert.ToString(_currentCulture))
        //    {
        //        case "tr-TR":
        //            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("tr-TR");
        //            GetResources();
        //            Languages[0].Content = cbxEnglish;
        //            Languages[1].Content = cbxTurkish;
        //            //Languages[2].Content = cbxItalian;


        //            break;

        //        case "en-US":
        //            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
        //            GetResources();
        //            Languages[0].Content = cbxEnglish;
        //            Languages[1].Content = cbxTurkish;
        //            //Languages[2].Content = cbxItalian;

        //            break;

        //    }
        //}

        //private void GetResources()
        //{
        //    LibraryPath = Resources.PathOfLibrary;
        //    ToolDbPath = Resources.DataBase;
        //    ToolPath = Resources.PathOfTools;
        //    Helper = Resources.Helper;
        //    txtTimer = Resources.txtTimer;
        //    txtLanguage = Resources.txtLanguage;
        //    txtHelper = Resources.txtHelper;
        //    txtUpdate = Resources.txtUpdate;
        //    cbxEnglish = Resources.cbxEnglish;
        //    cbxTurkish = Resources.cbxTurkish;
        //    //cbxItalian = Resources.cbxItalian;
        //    txtProgression = Resources.txtProgression;

        //    msgFile = Resources.msgFile;
        //}
        private string _txtLanguage;

        public string txtLanguage
        {
            get => _txtLanguage;
            set { _txtLanguage = value; OnPropertyChanged(nameof(txtLanguage)); }
        }
        private ObservableCollection<Language> _languages;
        public ObservableCollection<Language> Languages
        {
            get => _languages;
            set
            {
                _languages = value;
               
                OnPropertyChanged(nameof(Languages));

            }
        }
        



       
        private string _cbxEnglish;
        public string cbxEnglish
        {
            get => _cbxEnglish;
            set
            {
                _cbxEnglish = value;
                OnPropertyChanged(nameof(cbxEnglish));
            }
        }
        //private string _cbxItalian;

        //public string cbxItalian
        //{
        //    get { return _cbxItalian; }
        //    set
        //    {
        //        _cbxItalian = value;
        //        OnPropertyChanged(nameof(cbxItalian));
        //    }
        //}
        private string _cbxTurkish;

        public string cbxTurkish
        {
            get => _cbxTurkish;
            set
            {
                _cbxTurkish = value;
                OnPropertyChanged(nameof(cbxTurkish));
            }
        }
       
    }
}
