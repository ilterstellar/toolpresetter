﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolDataLibraryAsync.Enums
{
    public enum EnumDbToolMaterial
    {
        HSS,
        Carbide,
        Ceramic
    }
}
