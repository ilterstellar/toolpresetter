﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolDataLibraryAsync.Enums
{
    public enum EnumDbToolInOut : ushort
    {
        OutActive,
        OutBroken,
        ShelfActive,
        ShelfBroken,
        MagazineActive,
        MagazineBroken,
        WSSToolActive,
        WSSToolBroken,
        AbortedShelf,
        AbortedWSS,
        FinishToolLife,
        Empty,
        AbortedMagazine,
        Holder,
        WssHolder,
        Disable,
        ToolLifeWarning,
        Passive,
        FinishToolLifeShelf

    }
}
