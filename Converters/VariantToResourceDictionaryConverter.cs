﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ToolDataLibraryAsync.Converters
{
    public class VariantToResourceDictionaryConverter:IValueConverter
    {
        //public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        //{
        //    if (value is string variant)
        //    {
        //        string stylePath = GetStylePathFromVariant(variant);
        //        return stylePath;
        //    }

        //    // Return a default style if the variant is not recognized
        //    return "Styles/WhiteResourceDictionary.xaml";
        //}

        //public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        //{
        //    throw new NotSupportedException();
        //}

        //private string GetStylePathFromVariant(string variant)
        //{
        //    switch (variant)
        //    {
        //        case "White":
        //            return "Styles/WhiteResourceDictionary.xaml";
        //        case "Violet":
        //            return "Styles/VioletResourceDictionary.xaml";
        //        case "MilkyBlue":
        //            return "Styles/BlueResourceDictionary.xaml";
        //        case "MilkyGreen":
        //            return "Styles/GreenResourceDictionary.xaml";
        //        default:
        //            return "Styles/WhiteResourceDictionary.xaml";
        //    }
        //}
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var a = Application.Current.Resources.Keys.Count;

            // Iterate over the resource names
            
            if (value is string variant)
            {
                if (variant == "White")
                    return (Style)Application.Current.Resources[parameter];
                else if (variant == "MilkyViolet")
                    return Application.Current.Resources["VioletResourceDictionary"];
                else if (variant == "MilkyBlue")
                    return Application.Current.Resources["BlueResourceDictionary"];
                else if (variant == "MilkyGreen")
                    return Application.Current.Resources["GreenResourceDictionary"];
            }

            // Return a default resource dictionary if the variant is not recognized
            return Application.Current.Resources["DefaultResourceDictionary"];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
