﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ToolDataLibraryAsync.Converters
{
    public class ChangeStyle
    {
        public static void ChangeTheme(Uri styleUri)
        {
            ResourceDictionary Theme = new ResourceDictionary(){Source = styleUri};
            var a = Application.Current.Resources.MergedDictionaries;
            Application.Current.Resources.MergedDictionaries.Clear();
            Application.Current.Resources.MergedDictionaries.Add(Theme);
            
        }
    }
}
