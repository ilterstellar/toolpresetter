﻿using System;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace ToolDataLibraryAsync.Converters
{
    public class SettingsViewcbxStyleConverter:IValueConverter
    {
        private static readonly string WorkingDirectory = Environment.CurrentDirectory;
        private readonly string _projectDirectory = Directory.GetParent(WorkingDirectory)?.Parent?.FullName;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string styleName)
            {
                string filePath = $"{_projectDirectory}\\Styles\\SettingsStyle.xaml";

                using (FileStream stream = new FileStream(filePath, FileMode.Open))
                {
                    ResourceDictionary dictionary = (ResourceDictionary)XamlReader.Load(stream);

                    if (styleName == "White")
                        return dictionary["MilkyWhiteComboboxStyle"];
                    if (styleName == "Blue")
                        return dictionary["MilkyBlueComboboxStyle"];
                    if (styleName == "Violet")
                        return dictionary["MilkyVioletComboboxStyle"];
                    if (styleName == "Green")
                        return dictionary["MilkyGreenComboboxStyle"];
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
