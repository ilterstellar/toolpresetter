﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace ToolDataLibraryAsync.Converters
{
    public class BorderStyleConverter:IValueConverter
    {
        private static readonly string WorkingDirectory = Environment.CurrentDirectory;
        private readonly string _projectDirectory = Directory.GetParent(WorkingDirectory)?.Parent?.FullName;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string styleName)
            {
                string filePath = $"{_projectDirectory}\\Styles\\DataGridStyles.xaml";

                using (FileStream stream = new FileStream(filePath, FileMode.Open))
                {
                    ResourceDictionary dictionary = (ResourceDictionary)XamlReader.Load(stream);

                    if (styleName == "White")
                        return dictionary["Border-White"];
                    if (styleName == "Blue")
                        return dictionary["Border-Blue"];
                    if (styleName == "Violet")
                        return dictionary["Border-Violet"];
                    if (styleName == "Green")
                        return dictionary["Border-Green"];
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
