﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Markup;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualBasic.Devices;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ToolDataLibraryAsync.Data_Access.SQLite;
using ToolDataLibraryAsync.Data_Access.SQLite.SettingsDb;
using ToolDataLibraryAsync.Models;
using ToolDataLibraryAsync.ViewModels;

namespace ToolDataLibraryAsync.RabbitMQ
{
    public class RabbitMQClient: IRabbitMQClient
    {
        public ConnectionFactory _factory;
        public SettingsDal _settingsDal;
        public IConnection _connection;
        public bool mqConnection;
        private readonly IServiceProvider _serviceProvider;
        private IModel _channel;
        private dynamic data;
        private dynamic instance;
        private string type;
        private string command;
        private string computer;
        public string Response;

        public DbToolModel ResponseModel { get; set; }

        public RabbitMQClient(IServiceProvider serviceProvider)
        {
            _settingsDal = new SettingsDal();
            _serviceProvider = serviceProvider;
            //_factory = new ConnectionFactory()
            //{
            //    HostName = (string)_settingsDal.GetSetting("HostName"),
            //    Port = Convert.ToInt32(_settingsDal.GetSetting("MQPort")),
            //    ClientProvidedName = "ToolPresetter",
            //    UserName = "toolpresetter",
            //    Password = "toolpresetter",
            //};
            //CreateConn();
        }
        public void CreateConn(int Port)
        {
            try
            {
                _factory = new ConnectionFactory()
                {

                    HostName = (string)_settingsDal.GetSetting("HostName"),
                    Port = Port,
                    ClientProvidedName = "ToolPresetter",
                    UserName = "toolpresetter",
                    Password = "toolpresetter",
                };
                _connection = _factory.CreateConnection();

                //SendDataToMQ<DbToolModel>(command: "Get", model: "ID");
                _channel = _connection.CreateModel();
                _channel.ExchangeDeclare(exchange: "TPS_Exchange", type: ExchangeType.Direct);
                _channel.QueueDeclare(queue: "TPS_queue", durable: false, exclusive: false, autoDelete: false, arguments: null);
                _channel.QueueBind(queue: "TPS_queue", exchange: "TPS_Exchange", routingKey: "ILoader1");
                ConsumeJson("ILoader1").Wait();
                mqConnection = true;

            }
            catch (Exception ex)
            {
                mqConnection = false;
            }

        }
        public Task SendDataToMQ<T>(string command, object model=null, string computer = "ILoader1")
        {
            Task.Run(() => {
                PublishJson<T>(
                computer: computer,
                command: command,
                data: model,
                exchange: "Server_exchange",
                queueName: "Server_queue",
                routingKey: $"{computer}");
            });

            return Task.CompletedTask;

        }
        public void PublishJson<T>(string computer, string command, object data, string exchange = "Server_exchange", string queueName = "Server_queue", string routingKey = "ILoader1")
        {
            using (var _connection = _factory.CreateConnection())
            {
                using (var _channel = _connection.CreateModel())
                {

                    var message = new
                    {
                        Command = command,
                        ModelType = typeof(T).Name,
                        Computer = computer,
                        Data = data
                    };

                    var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(message));

                    // Declare exchange
                    _channel.ExchangeDeclare(exchange: exchange, type: ExchangeType.Direct);

                    // Declare and bind the queue
                    _channel.QueueDeclare(queue: queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);
                    _channel.QueueBind(queue: queueName, exchange: exchange, routingKey: routingKey);

                    // Publish the message
                    _channel.BasicPublish(exchange: exchange, routingKey: routingKey, basicProperties: null, body: body);
                }

            }

        }

        public async Task ConsumeJson(string routingKey)
        {
            try
            {
                var consumer = new EventingBasicConsumer(_channel);
                (string Command, string ModelType, string Computer, dynamic Data)? result = null;
                consumer.Received += (model, ea) =>
                {
                    try
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        var jsonObject = JsonDocument.Parse(message).RootElement;

                        if (jsonObject.TryGetProperty("Command", out var commandElement) &&
                            jsonObject.TryGetProperty("Data", out var dataElement) &&
                            jsonObject.TryGetProperty("ModelType", out var modelTypeElement) &&
                            jsonObject.TryGetProperty("Computer", out var computerElement))
                        {
                            command = commandElement.GetString();
                            type = modelTypeElement.GetString();
                            computer = computerElement.GetString();
                            data = JsonSerializer.Deserialize<dynamic>(dataElement.GetRawText());
                            switch (type)
                            {

                                case nameof(DbToolModel):                                    
                                    Process<DbToolModel>(data, type);
                                    break;
                                    default:
                                    break;
                            }
                            //result = (command, modelType, computer, data);

                            _channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                };

                _channel.BasicConsume(queue: "TPS_queue", autoAck: false, consumer: consumer);

            }
            catch (Exception ex)
            {
            }

        }
        public async Task Process<DbToolModel>(dynamic data, string type)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
              

                switch (command)
                {
                    case "Get":
                        instance  = JsonSerializer.Deserialize<DbToolModel>(data.ToString());
                        ResponseModel = instance;
                        RFIDViewModel.Instance.DbToolModel = instance;


                        break;
                    default:
                        break;
                }


            }
        }
    }
}
