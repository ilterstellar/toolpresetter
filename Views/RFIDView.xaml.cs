﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToolDataLibraryAsync.ViewModels;

namespace ToolDataLibraryAsync.Views
{
    /// <summary>
    /// Interaction logic for RFIDView.xaml
    /// </summary>
    public partial class RFIDView : Window
    {
        public RFIDView()
        {
            InitializeComponent();
            this.Show();
           
        }
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
        private void MinimizeButton_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
            RFIDViewModel.Instance = null;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Adjust the window position
            WindowStartupLocation = WindowStartupLocation.CenterScreen;

            // Enable window drag functionality

        }
    }
}
