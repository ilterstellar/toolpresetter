﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ToolDataLibraryAsync.Views
{
    /// <summary>
    /// Interaction logic for LicenseValidationView.xaml
    /// </summary>
    public partial class LicenseValidationView : Window
    {
        public LicenseValidationView()
        {
            InitializeComponent();
        }
        private void MinimizeButton_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //Handle This on VM
        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            
            var licenseKey = "";

            // Perform license key validation logic here
            // You can use the LicenseKey value for validation or pass it to another validation method

            // Example: Check if the license key is valid
            bool isValid = ValidateLicenseKey(licenseKey);

            if (isValid)
            {
                // License key is valid, perform login logic or navigate to the main application
                MessageBox.Show("License key is valid. Login successful!");
            }
            else
            {
                // License key is invalid, display an error message
                MessageBox.Show("Invalid license key. Login failed!");
            }
        }

        private bool ValidateLicenseKey(string licenseKey)
        {
            // Perform your license key validation logic here
            // Return true if the license key is valid, false otherwise

            // Example: Validate license key based on specific criteria
            return (licenseKey == "validLicenseKey");
        }
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Adjust the window position
            WindowStartupLocation = WindowStartupLocation.CenterScreen;

            // Enable window drag functionality
            MouseLeftButtonDown += (mouseEventSender, mouseEventArgs) =>
            {
                DragMove();
            };
        }
    }
}
