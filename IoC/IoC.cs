﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolDataLibraryAsync.RabbitMQ;
using ToolDataLibraryAsync.ToolCenterComm;

namespace ToolDataLibraryAsync
{
    public static class IoC
    {
        public static RabbitMQClient RabbitMQClient { get; set; } = null;
        public static IServiceCollection ServiceCollection { get; set; }
        public static IServiceProvider ServiceProvider { get; set; }
        public static ToolCenterCommunication Communication { get; set; }
        public static void InitializeDb()
        {
            
            ServiceCollection = new ServiceCollection();
            ServiceProvider = ServiceCollection.BuildServiceProvider();
            RabbitMQClient = new RabbitMQClient(ServiceProvider);
            Communication = new ToolCenterCommunication();

        }
    }
}
