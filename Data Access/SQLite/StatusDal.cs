﻿using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using ToolDataLibraryAsync.Models;
using System.Configuration;
using Microsoft.Data.Sqlite;

namespace ToolDataLibraryAsync.Data_Access.SQLite
{
    public class StatusDal:IDisposable
    {
        private static readonly string WorkingDirectory = Environment.CurrentDirectory;
        private readonly string _projectDirectory = Directory.GetParent(WorkingDirectory)?.Parent?.FullName;
        private DbConfig _dbConfig;
       
        public StatusDal()
        {  
            

            _dbConfig = new DbConfig();

            string configFilePath =
                $"{_projectDirectory}\\App.config";
            ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
            configFileMap.ExeConfigFilename = configFilePath;
            Configuration config =
                ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);

            CreateIfNotExists().Wait();


        }
        public async Task CreateIfNotExists()
        {
                await CreateStatusTable();

            if (await IsStatusTableEmpty())
            {
                var newStatus = new ToolDataLibraryStatus
                {
                    Name = "Status",
                    LibraryPath = "",
                    ToolPath = "",
                    ToolDbPath = "",
                    LibraryPathBool = true,
                    ToolPathBool = true,
                    TimeBool = false,
                    Lang = "tr-TR",
                    TimeInterval = 10,
                };

                await InsertStatus(newStatus);
            }
        }
        private async Task CreateStatusTable()
        {
            // Define the SQL query to create the ToolDataLibraryStatus table
            string createTableQuery = "CREATE TABLE IF NOT EXISTS ToolDataLibraryStatus (" +
                                      "Id INTEGER PRIMARY KEY AUTOINCREMENT," +
                                      "Name TEXT NOT NULL," +
                                      "LibraryPath TEXT," +
                                      "ToolPath TEXT," +
                                      "ToolDbPath TEXT," +
                                      "LibraryPathBool INTEGER," +
                                      "ToolPathBool INTEGER," +
                                      "TimeBool INTEGER," +
                                      "Lang TEXT," +
                                      "TimeInterval INTEGER" +
                                      ");";

            using (var command = new SqliteCommand(createTableQuery, _dbConfig.SqliteConn))
            {
                await command.ExecuteNonQueryAsync();
            }
        }
        public async Task<bool> IsStatusTableEmpty()
        {
            using (SqliteCommand command = new SqliteCommand("SELECT COUNT(*) FROM ToolDataLibraryStatus", _dbConfig.SqliteConn))
            {
                long rowCount = (long)await command.ExecuteScalarAsync();
                return rowCount == 0;
            }
        }
        private async Task InsertStatus(ToolDataLibraryStatus status)
        {
            string query = "INSERT INTO ToolDataLibraryStatus (Name, LibraryPath, ToolPath, ToolDbPath, LibraryPathBool, ToolPathBool, TimeBool, Lang, TimeInterval) " +
                           "VALUES (@Name, @LibraryPath, @ToolPath, @ToolDbPath, @LibraryPathBool, @ToolPathBool, @TimeBool, @Lang, @TimeInterval)";

            using (var command = new SqliteCommand(query, _dbConfig.SqliteConn))
            {
                command.Parameters.AddWithValue("@Name", status.Name);
                command.Parameters.AddWithValue("@LibraryPath", status.LibraryPath);
                command.Parameters.AddWithValue("@ToolPath", status.ToolPath);
                command.Parameters.AddWithValue("@ToolDbPath", status.ToolDbPath);
                command.Parameters.AddWithValue("@LibraryPathBool", status.LibraryPathBool);
                command.Parameters.AddWithValue("@ToolPathBool", status.ToolPathBool);
                command.Parameters.AddWithValue("@TimeBool", status.TimeBool);
                command.Parameters.AddWithValue("@Lang", status.Lang);
                command.Parameters.AddWithValue("@TimeInterval", status.TimeInterval);

                await command.ExecuteNonQueryAsync();
            }
        }
        public async Task<ToolDataLibraryStatus> GetAllStatus()
        {



            SqliteCommand command = new SqliteCommand("SELECT * FROM ToolDataLibraryStatus", _dbConfig.SqliteConn);
            SqliteDataReader reader = command.ExecuteReader();

                reader.Read();
            
                var status = new ToolDataLibraryStatus
                {
                    Name = reader["Name"].ToString(),
                    LibraryPath = reader["LibraryPath"].ToString(),
                    ToolPath = reader["ToolPath"].ToString(),
                    ToolDbPath = reader["ToolDbPath"].ToString(),
                    LibraryPathBool = Convert.ToBoolean(reader["LibraryPathBool"]),
                    ToolPathBool = Convert.ToBoolean(reader["ToolPathBool"]),
                    TimeBool = Convert.ToBoolean(reader["TimeBool"]),
                    Lang = reader["Lang"].ToString(),
                    TimeInterval = Convert.ToInt32(reader["TimeInterval"])

                };
            

            reader.Close();
            await command.ExecuteNonQueryAsync();

            return status;
        }

        object _stats;
        public Object GetStatus(string column)
        { 
            
            SqliteCommand command = new SqliteCommand($"SELECT {column} FROM ToolDataLibraryStatus", _dbConfig.SqliteConn);
            SqliteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                _stats = reader[column].ToString();
            }
            reader.Close();

            return _stats;
        }
        public void UpdateAllStatus(ToolDataLibraryStatus status)
        {
            SqliteCommand command = 
                new SqliteCommand(
                    "UPDATE  ToolDataLibraryStatus SET LibraryPath=@LibraryPath, ToolPath=@ToolPath, ToolDbPath=@ToolDbPath, LibraryPathBool=@LibraryPathBool, ToolPathBool=@ToolPathBool, TimeBool=@TimeBool, Lang=@Lang, TimeInterval=@TimeInterval  WHERE Name=@Name",
                    _dbConfig.SqliteConn);
          
            command.Parameters.AddWithValue("@Name","Status");
            command.Parameters.AddWithValue("@LibraryPath", status.LibraryPath);
            command.Parameters.AddWithValue("@ToolPath", status.ToolPath);
            command.Parameters.AddWithValue("@ToolDbPath", status.ToolDbPath);
            command.Parameters.AddWithValue("@LibraryPathBool", status.LibraryPathBool);
            command.Parameters.AddWithValue("@ToolPathBool", status.ToolPathBool);
            command.Parameters.AddWithValue("@TimeBool", status.TimeBool);
            command.Parameters.AddWithValue("@Lang", status.Lang);
            command.Parameters.AddWithValue("@TimeInterval", status.TimeInterval);
       


            //kayıt olup olmadı mı kontrolü ? olduysa 1 : olmadıysa 0, return eder.
            command.ExecuteNonQuery();




        }
        public async Task UpdateStatus(string column, object selection)
        {
            string query = "UPDATE ToolDataLibraryStatus SET " + column + "=@value WHERE Name=@name";

            using (var command = new SqliteCommand(query, _dbConfig.SqliteConn))
            {
                command.Parameters.AddWithValue("@value",value:selection);
                command.Parameters.Add("@name", (SqliteType)DbType.String).Value = "Status"; 
               
                await command.ExecuteNonQueryAsync();
            }
        }
        public void Dispose()
        {
           GC.SuppressFinalize(this);
        }

    }
}
