﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using ToolDataLibraryAsync.Models;

namespace ToolDataLibraryAsync.Data_Access.SQLite
{
    public class ToolDataDal : IDisposable
    {
        private static readonly string WorkingDirectory = Environment.CurrentDirectory;
        private readonly string _projectDirectory = Directory.GetParent(WorkingDirectory)?.Parent?.FullName;
        private DbConfig _dbConfig;
        public ToolDataDal()
        {
            _dbConfig = new DbConfig();
            string configFilePath =
                $"{_projectDirectory}\\App.config";
            ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
            configFileMap.ExeConfigFilename = configFilePath;
            Configuration config =
                ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);
           
            CreateToolsTableIfNotExists().Wait();
        }
        public async Task CreateToolsTableIfNotExists()
        {
            using (SqliteCommand command = new SqliteCommand(
                "CREATE TABLE IF NOT EXISTS Toolsdb (" +
                "Id2 INTEGER PRIMARY KEY, " +
                "Number TEXT, " +
                "Name TEXT, " +
                "Radius REAL, " +
                "Diameter REAL, " +
                "CoordinateZ REAL, " +
                "TheoreticalRadius REAL, " +
                "TheoreticalZ REAL," +
                "LifeOf TEXT, " +
                "CutterEdgeRadius REAL, " +
                "CutterEdgeOrient INTEGER, " +      
                "Type INTEGER, " +
                "CNCName TEXT)",
                _dbConfig.SqliteConn))
            {
                await command.ExecuteNonQueryAsync();
            }
        }
        public decimal GetOneTool(int receivedID)
        {
            string cmdline = $"SELECT * FROM MachineDatadb WHERE IdNumber={receivedID}";
            SqliteCommand command = new SqliteCommand(cmdline, _dbConfig.SqliteConn);
            decimal length;
            SqliteDataReader reader = command.ExecuteReader();
            reader.Read();
            length = reader[5] != DBNull.Value ? Convert.ToDecimal(reader[5]) : 0;
            return length;
        }
        public async Task CreateToolDT(string NameOfCNC)
        {
                SqliteCommand command = new SqliteCommand($"CREATE TABLE IF NOT EXISTS {NameOfCNC}" +
                                                          $"(Id2 VARCHAR(50) NOT NULL PRIMARY KEY," +
                                                          $"Number VARCHAR (50)," +
                                                          $"Name VARCHAR (50)," +
                                                          $"Radius DECIMAL(18, 4)," +
                                                          $"Diameter DECIMAL(18, 4)," +
                                                          $"Z DECIMAL (18, 4)," +
                                                          $"TheoreticalRadius DECIMAL(18, 4)," +
                                                          $"TheoreticalZ DECIMAL (18, 4)," +
                                                          $"Lifeof VARCHAR(50)," +
                                                          $"CutterEdgeRadius DECIMAL(18, 4)," +
                                                          $"CutterEdgeOrient INT," +
                                                          $"Type INT," +
                                                          $"CNCName VARCHAR(50))", _dbConfig.SqliteConn);
                await command.ExecuteNonQueryAsync();     
    }
        public async Task<List<Tool>> GetAllTools()
        {
            SqliteCommand command = new SqliteCommand("SELECT * FROM Toolsdb", _dbConfig.SqliteConn);
            SqliteDataReader reader = command.ExecuteReader();
            List<Tool> toolList = new List<Tool>();
            while (await reader.ReadAsync())
            {
                var tool = new Tool
                {
                    Id = Convert.ToInt32(reader["Id2"]),
                    Name = reader["Name"].ToString(),
                    CoordinateZ = Convert.ToDouble(reader["CoordinateZ"]),
                    CutterEdgeOrient = Convert.ToInt32(reader["CutterEdgeOrient"]),
                    CutterEdgeRadius = Convert.ToDouble(reader["CutterEdgeRadius"]),
                    Diameter = Convert.ToDouble(reader["Diameter"]),
                    LifeOf = reader["Lifeof"].ToString(),
                    Number = reader["Number"].ToString(),
                    Radius = Convert.ToDouble(reader["Radius"]),
                    TheoreticalRadius = Convert.ToDouble(reader["TheoreticalRadius"]),
                    TheoreticalZ = Convert.ToDouble(reader["TheoreticalZ"]),
                    Type = Convert.ToInt32(reader["Type"]),
                    CNCName = reader["CNCName"].ToString()
                };
                toolList.Add(tool);
            }
            reader.Close();
            return toolList;
        }
        public async Task Add(Tool tool,string cncName)
        {
            try
            { 
                SqliteCommand command =
                    new SqliteCommand(
                        $"INSERT INTO Toolsdb SELECT @Id2, @Number, @Name, @Radius, @Diameter, @CoordinateZ, @TheoreticalRadius, @TheoreticalZ, @LifeOf, @CutterEdgeRadius, @CutterEdgeOrient, @Type, @CNCName WHERE NOT EXISTS (SELECT 1 FROM Toolsdb WHERE Id2 = @Id2)"
                        , _dbConfig.SqliteConn);
                command.Parameters.AddWithValue("@Id2", tool.Id);
                command.Parameters.AddWithValue("@Number", tool.Number);
                command.Parameters.AddWithValue("@Name", tool.Name);
                command.Parameters.AddWithValue("@Radius", tool.Radius);
                command.Parameters.AddWithValue("@Diameter", tool.Diameter);
                command.Parameters.AddWithValue("@CoordinateZ", tool.CoordinateZ);
                command.Parameters.AddWithValue("@TheoreticalRadius", tool.TheoreticalRadius);
                command.Parameters.AddWithValue("@TheoreticalZ", tool.TheoreticalZ);
                command.Parameters.AddWithValue("@LifeOf", tool.LifeOf);
                command.Parameters.AddWithValue("@CutterEdgeRadius", tool.CutterEdgeRadius);
                command.Parameters.AddWithValue("@CutterEdgeOrient", tool.CutterEdgeOrient);
                command.Parameters.AddWithValue("@Type", tool.Type);
                command.Parameters.AddWithValue("@CNCName", cncName);
                await command.ExecuteNonQueryAsync();
            }
            catch
            {
             //ignored   
            }
        }
        public async Task AddToSubDT(Tool tool, string cncName)
        {
            try
            {
                SqliteCommand command =
                new SqliteCommand(
                    $"INSERT INTO {cncName} SELECT @Id2, @Number, @Name, @Radius, @Diameter, @CoordinateZ, @TheoreticalRadius, @TheoreticalZ, @LifeOf, @CutterEdgeRadius, @CutterEdgeOrient, @Type, @CNCName WHERE NOT EXISTS (SELECT 1 FROM {cncName} WHERE Id2 = @Id2)"
                    , _dbConfig.SqliteConn);
            command.Parameters.AddWithValue("@Id2", tool.Id);
            command.Parameters.AddWithValue("@Number", tool.Number);
            command.Parameters.AddWithValue("@Name", tool.Name);
            command.Parameters.AddWithValue("@Radius", tool.Radius);
            command.Parameters.AddWithValue("@Diameter", tool.Diameter);
            command.Parameters.AddWithValue("@CoordinateZ", tool.CoordinateZ);
            command.Parameters.AddWithValue("@TheoreticalRadius", tool.TheoreticalRadius);
            command.Parameters.AddWithValue("@TheoreticalZ", tool.TheoreticalZ);
            command.Parameters.AddWithValue("@LifeOf", tool.LifeOf);
            command.Parameters.AddWithValue("@CutterEdgeRadius", tool.CutterEdgeRadius);
            command.Parameters.AddWithValue("@CutterEdgeOrient", tool.CutterEdgeOrient);
            command.Parameters.AddWithValue("@Type", tool.Type);
            command.Parameters.AddWithValue("@CNCName", cncName);
            //kayıt olup olmadı mı kontrolü ? olduysa 1 : olmadıysa 0, return eder.
            await command.ExecuteNonQueryAsync();
            }
            catch
            {
                //ignored   
            }
        }
        public async Task AddAll(List<Tool> tools, string cncName="")
        {
            foreach (var tool in tools)
            {
                await Add(tool, cncName);
                await AddToSubDT(tool, cncName);
            }
        }
        public async Task UpdateAll(List<Tool> Tools
        )
        {
            foreach (var tool in Tools)
            {
              await UpdateTools(tool);
            }

        }
        public async Task UpdateTools(Tool tool)
        {
            SqliteCommand command =
                new SqliteCommand(
                    "UPDATE Toolsdb SET Number=@Number, Name=@Name, TheoreticalRadius=@TheoreticalRadius, TheoreticalZ=@TheoreticalZ, LifeOf=@LifeOf, CutterEdgeRadius=@CutterEdgeRadius, CutterEdgeOrient=@CutterEdgeOrient, Type=@Type, Radius=@Radius, Diameter=@Diameter, CoordinateZ=@CoordinateZ, CNCName=@CNCName WHERE Id2=@id2",
                    _dbConfig.SqliteConn);
            command.Parameters.AddWithValue("@id2", tool.Id);
            command.Parameters.AddWithValue("@Number", tool.Number);
            command.Parameters.AddWithValue("@Name", tool.Name);
            command.Parameters.AddWithValue("@Radius", tool.Radius);
            command.Parameters.AddWithValue("@Diameter", tool.Diameter);
            command.Parameters.AddWithValue("@CoordinateZ", tool.CoordinateZ);
            command.Parameters.AddWithValue("@TheoreticalRadius", tool.TheoreticalRadius);
            command.Parameters.AddWithValue("@TheoreticalZ", tool.TheoreticalZ);
            command.Parameters.AddWithValue("@LifeOf", tool.LifeOf);
            command.Parameters.AddWithValue("@CutterEdgeRadius", tool.CutterEdgeRadius);
            command.Parameters.AddWithValue("@CutterEdgeOrient", tool.CutterEdgeOrient);
            command.Parameters.AddWithValue("@Type", tool.Type);
            command.Parameters.AddWithValue("@CNCName", tool.CNCName);


            //kayıt olup olmadı mı kontrolü ? olduysa 1 : olmadıysa 0, return eder.
           await command.ExecuteNonQueryAsync();
        }
        public async Task DeleteAll()
        {
            SqliteCommand command = new SqliteCommand("DELETE FROM Toolsdb", _dbConfig.SqliteConn);
            await command.ExecuteNonQueryAsync();
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }  
    }
}
