﻿using System;
using System.Collections.Generic;
using Microsoft.Data.Sqlite;
using System.IO;
using ToolDataLibraryAsync.Models;
using System.Configuration;
using System.Threading.Tasks;

namespace ToolDataLibraryAsync.Data_Access.SQLite
{
        public class MachineDataDal : IDisposable
        {
            private static readonly string WorkingDirectory = Environment.CurrentDirectory;
            private readonly string _projectDirectory = Directory.GetParent(WorkingDirectory)?.Parent?.FullName;

            private DbConfig _dbConfig;
            public MachineDataDal()
            {
                _dbConfig = new DbConfig();

                string configFilePath =
                    $"{_projectDirectory}\\App.config";
                ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
                configFileMap.ExeConfigFilename = configFilePath;
                Configuration config =
                    ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);
                //_path = $"DataSource={config.AppSettings.Settings["MachineDataDbPath"].Value}; Version=3";
                CreateMachineDataTableIfNotExists();

        }

        public void CreateMachineDataTableIfNotExists()
        {
            SqliteCommand command = new SqliteCommand(@"
                    CREATE TABLE IF NOT EXISTS MachineDataDb (" +
        "Tool TEXT, " +
        "IdNumber INTEGER, " +
        "ToolType TEXT, " +
        "ToolUserType TEXT, " +
        "Permanent INTEGER, " +
        "UnitsDiameter TEXT, " +
        "Diameter REAL, " +
        "Angle REAL, " +
        "Radius REAL, " +
        "ProfileRadius REAL, " +
        "UpperRadius REAL, " +
        "NumTeeth INTEGER, " +
        "Description TEXT, " +
        "TaperAngle REAL, " +
        "ShankDiameter REAL, " +
        "UnitsLength TEXT, " +
        "Length REAL, " +
        "TotalLength INTEGER, " +
        "ShoulderLength INTEGER, " +
        "Startshoulderlength INTEGER, " +
        "TipLength INTEGER, " +
        "CuttingLength REAL, " +
        "HLength INTEGER, " +
        "Material TEXT, " +
        "UnitsFeedSpin TEXT, " +
        "FType TEXT, " +
        "FeedXY REAL, " +
        "FeedZ REAL, " +
        "FeedFinish REAL, " +
        "SType TEXT, " +
        "Spin REAL, " +
        "SpinFinish REAL, " +
        "FeedZPenetration TEXT, " +
        "FeedLeadIn INTEGER, " +
        "FeedLeadOut INTEGER, " +
        "FeedLink INTEGER, " +
        "ToolName TEXT, " +
        "ToolGroup TEXT, " +
        "HolderName TEXT, " +
        "GroupHolderName TEXT, " +
        "Direction TEXT, " +
        "ChamferLength INTEGER, " +
        "TipDiameter REAL, " +
        "ShoulderDiameter REAL, " +
        "ShoulderAngle REAL, " +
        "Message1 TEXT, " +
        "Message2 TEXT, " +
        "Message3 TEXT, " +
        "Message4 TEXT, " +
        "Message5 TEXT, " +
        "FloodCoolant INTEGER, " +
        "MistCoolant INTEGER, " +
        "HighPressureCoolant INTEGER, " +
        "LowPressureCoolant INTEGER, " +
        "ThroughHighPressureCoolant INTEGER, " +
        "ThroughLowPressureCoolant INTEGER, " +
        "AirBlastCoolant INTEGER, " +
        "MinimumQuantityLubricationCoolant INTEGER, " +
        "MinimumQuantityLubricationValue INTEGER, " +
        "Rough INTEGER, " +
        "SpinDirection TEXT, " +
        "SpinLimit INTEGER, " +
        "ThreadingStandard TEXT, " +
        "ThreadStandardByUser TEXT, " +
        "ThreadingTableStandard TEXT, " +
        "NumThreads INTEGER, " +
        "Pitch REAL, " +
        "PitchUnit TEXT, " +
        "CNCName TEXT)", DbConfig.Instance.SqliteConn);
            command.ExecuteNonQuery();

        }
        public async Task CreateDT(string NameOfCNC)
            {
                SqliteCommand command = new SqliteCommand($"CREATE TABLE {NameOfCNC}( IdNumber INT,   Tool VARCHAR(50),  Radius FLOAT)", _dbConfig.SqliteConn);
                await command.ExecuteNonQueryAsync();
        }

            public async Task Add(MachineData machineData,string cncName)
            {
                
                SqliteCommand command =
                    new SqliteCommand(
                        @"INSERT INTO MachineDatadb 
                                VALUES(@Tool,
    @IdNumber,
    @ToolType,
    @ToolUserType,
    @Permanent,
    @UnitsDiameter,
    @Diameter,
    @Angle,
    @Radius,
    @ProfileRadius,
    @UpperRadius,
    @NumTeeth,
    @Description,
    @TaperAngle,
    @ShankDiameter,
    @UnitsLength,
    @Length,
    @TotalLength,
    @ShoulderLength,
    @Startshoulderlength,
    @TipLength,
    @CuttingLength,
    @HLength,
    @Material,
    @UnitsFeedSpin,
    @FType,
    @FeedXY,
    @FeedZ,
    @FeedFinish,
    @SType,
    @Spin,
    @SpinFinish,
    @FeedZPenetration,
    @FeedLeadIn,
    @FeedLeadOut,
    @FeedLink,
    @ToolName,
    @ToolGroup,
    @HolderName,
    @GroupHolderName,
    @Direction,
    @ChamferLength,
    @TipDiameter,
    @ShoulderDiameter,
    @ShoulderAngle,
    @Message1,
    @Message2,
    @Message3,
    @Message4,
    @Message5,
    @FloodCoolant,
    @MistCoolant,
    @HighPressureCoolant,
    @LowPressureCoolant,
    @ThroughHighPressureCoolant,
    @ThroughLowPressureCoolant,
    @AirBlastCoolant,
    @MinimumQuantityLubricationCoolant,
    @MinimumQuantityLubricationValue,
    @Rough,
    @SpinDirection,
    @SpinLimit,
    @ThreadingStandard,
    @ThreadStandardByUser,
    @ThreadingTableStandard,
    @NumThreads,
    @Pitch,
    @PitchUnit,
@CNCName)",
                        _dbConfig.SqliteConn);



                command.Parameters.AddWithValue("@Tool", machineData.Tool);
                command.Parameters.AddWithValue("@IdNumber", machineData.IdNumber);
                command.Parameters.AddWithValue("@ToolType", machineData.ToolType);
                command.Parameters.AddWithValue("@ToolUserType", machineData.ToolUserType);
                command.Parameters.AddWithValue("@Permanent", machineData.Permanent);
                command.Parameters.AddWithValue("@UnitsDiameter", machineData.UnitsDiameter);
                command.Parameters.AddWithValue("@Diameter", machineData.Diameter);
                command.Parameters.AddWithValue("@Angle", machineData.Angle);
                command.Parameters.AddWithValue("@Radius", machineData.Radius);
                command.Parameters.AddWithValue("@ProfileRadius", machineData.ProfileRadius);
                command.Parameters.AddWithValue("@UpperRadius", machineData.UpperRadius);
                command.Parameters.AddWithValue("@NumTeeth", machineData.NumTeeth);
                command.Parameters.AddWithValue("@Description", machineData.Description);
                command.Parameters.AddWithValue("@TaperAngle", machineData.TaperAngle);
                command.Parameters.AddWithValue("@ShankDiameter", machineData.ShankDiameter);
                command.Parameters.AddWithValue("@UnitsLength", machineData.UnitsLength);
                command.Parameters.AddWithValue("@Length", machineData.Length);
                command.Parameters.AddWithValue("@TotalLength", machineData.TotalLength);
                command.Parameters.AddWithValue("@ShoulderLength", machineData.ShoulderLength);
                command.Parameters.AddWithValue("@Startshoulderlength", machineData.Startshoulderlength);
                command.Parameters.AddWithValue("@TipLength", machineData.TipLength);
                command.Parameters.AddWithValue("@CuttingLength", machineData.CuttingLength);
                command.Parameters.AddWithValue("@HLength", machineData.HLength);
                command.Parameters.AddWithValue("@Material", machineData.Material);
                command.Parameters.AddWithValue("@UnitsFeedSpin", machineData.UnitsFeedSpin);
                command.Parameters.AddWithValue("@FType", machineData.FType);
                command.Parameters.AddWithValue("@FeedXY", machineData.FeedXY);
                command.Parameters.AddWithValue("@FeedZ", machineData.FeedZ);
                command.Parameters.AddWithValue("@FeedFinish", machineData.FeedFinish);
                command.Parameters.AddWithValue("@SType", machineData.SType);
                command.Parameters.AddWithValue("@Spin", machineData.Spin);
                command.Parameters.AddWithValue("@SpinFinish", machineData.SpinFinish);
                command.Parameters.AddWithValue("@FeedZPenetration", machineData.FeedZPenetration);
                command.Parameters.AddWithValue("@FeedLeadIn", machineData.FeedLeadIn);
                command.Parameters.AddWithValue("@FeedLeadOut", machineData.FeedLeadOut);
                command.Parameters.AddWithValue("@FeedLink", machineData.FeedLink);
                command.Parameters.AddWithValue("@ToolName", machineData.ToolName);
                command.Parameters.AddWithValue("@ToolGroup", machineData.ToolGroup);
                command.Parameters.AddWithValue("@HolderName", machineData.HolderName);
                command.Parameters.AddWithValue("@GroupHolderName", machineData.GroupHolderName);
                command.Parameters.AddWithValue("@Direction", machineData.Direction);
                command.Parameters.AddWithValue("@ChamferLength", machineData.ChamferLength);
                command.Parameters.AddWithValue("@TipDiameter", machineData.TipDiameter);
                command.Parameters.AddWithValue("@ShoulderDiameter", machineData.ShoulderDiameter);
                command.Parameters.AddWithValue("@ShoulderAngle", machineData.ShoulderAngle);
                command.Parameters.AddWithValue("@Message1", machineData.Message1);
                command.Parameters.AddWithValue("@Message2", machineData.Message2);
                command.Parameters.AddWithValue("@Message3", machineData.Message3);
                command.Parameters.AddWithValue("@Message4", machineData.Message4);
                command.Parameters.AddWithValue("@Message5", machineData.Message5);
                command.Parameters.AddWithValue("@FloodCoolant", machineData.FloodCoolant);
                command.Parameters.AddWithValue("@MistCoolant", machineData.MistCoolant);
                command.Parameters.AddWithValue("@HighPressureCoolant", machineData.HighPressureCoolant);
                command.Parameters.AddWithValue("@LowPressureCoolant", machineData.LowPressureCoolant);
                command.Parameters.AddWithValue("@ThroughHighPressureCoolant", machineData.ThroughHighPressureCoolant);
                command.Parameters.AddWithValue("@ThroughLowPressureCoolant", machineData.ThroughLowPressureCoolant);
                command.Parameters.AddWithValue("@AirBlastCoolant", machineData.AirBlastCoolant);
                command.Parameters.AddWithValue("@MinimumQuantityLubricationCoolant", machineData.MinimumQuantityLubricationCoolant);
                command.Parameters.AddWithValue("@MinimumQuantityLubricationValue", machineData.MinimumQuantityLubricationValue);
                command.Parameters.AddWithValue("@Rough", machineData.Rough);
                command.Parameters.AddWithValue("@SpinDirection", machineData.SpinDirection);
                command.Parameters.AddWithValue("@SpinLimit", machineData.SpinLimit);
                command.Parameters.AddWithValue("@ThreadingStandard", machineData.ThreadingStandard);
                command.Parameters.AddWithValue("@ThreadStandardByUser", machineData.ThreadStandardByUser);
                command.Parameters.AddWithValue("@ThreadingTableStandard", machineData.ThreadingTableStandard);
                command.Parameters.AddWithValue("@NumThreads", machineData.NumThreads);
                command.Parameters.AddWithValue("@Pitch", machineData.Pitch);
                command.Parameters.AddWithValue("@PitchUnit", machineData.PitchUnit);
                command.Parameters.AddWithValue("@CNCName", cncName);






                //kayıt olup olmadı mı kontrolü ? olduysa 1 : olmadıysa 0, return eder.
               await command.ExecuteNonQueryAsync();

              






            }
            public async Task AddAll( List<MachineData> machineDatas, string cncName = "")
            {
                foreach (var machineData in machineDatas)
                {
                    await Add(machineData, cncName);
                }
            }
            public async Task DeleteAll()
            {
              

                SqliteCommand command = new SqliteCommand("DELETE FROM MachineDatadb", _dbConfig.SqliteConn);

                await command.ExecuteNonQueryAsync();
               
            }
            public async Task<List<MachineData>> GetAllMachineData()
            {
                
                //if (_connection.State != ConnectionState.Open) { _connection.Open(); }

                SqliteCommand command = new SqliteCommand("SELECT * FROM MachineDatadb", _dbConfig.SqliteConn);
                SqliteDataReader reader = command.ExecuteReader();

                List<MachineData> machineDataList = new List<MachineData>();
                while (await reader.ReadAsync())
                {
                    var machineData = new MachineData();
                    machineData.Tool = reader[0] != DBNull.Value ? (string)reader[0] : "";
                    machineData.IdNumber = reader[1] != DBNull.Value ? Convert.ToInt32(reader[1]) : 0;
                    machineData.ToolType = reader[2] != DBNull.Value ? (string)reader[2] : "";
                    machineData.ToolUserType = reader[3] != DBNull.Value ? (string)reader[3] : "";
                     machineData.Permanent = reader[4] != DBNull.Value ? Convert.ToInt32(reader[4]) : 0;
                    machineData.UnitsDiameter = reader[5] != DBNull.Value ? (string)reader[5] : "";
                    machineData.Diameter = reader[6] != DBNull.Value ? Convert.ToDecimal(reader[6]) : 0;
                    machineData.Angle = reader[7] != DBNull.Value ? Convert.ToDecimal(reader[7]) : 0;
                    machineData.Radius = reader[8] != DBNull.Value ? Convert.ToDouble(reader[8]) : 0;
                    machineData.ProfileRadius = reader[9] != DBNull.Value ? Convert.ToInt32(reader[9]) : 0;
                    machineData.UpperRadius = reader[10] != DBNull.Value ? Convert.ToInt32(reader[10]) : 0;
                    machineData.NumTeeth = reader[11] != DBNull.Value ? Convert.ToInt32(reader[11]) : 0;
                    machineData.Description = reader[12] != DBNull.Value ? (string)reader[12] : "";
                    machineData.TaperAngle = reader[13] != DBNull.Value ? Convert.ToInt32(reader[13]) : 0;
                    machineData.ShankDiameter = reader[14] != DBNull.Value ? Convert.ToInt32(reader[14]) : 0;
                    machineData.UnitsLength = reader[15] != DBNull.Value ? (string)reader[15] : "";
                    machineData.Length = reader[16] != DBNull.Value ? Convert.ToDecimal(reader[16]) : 0;
                    machineData.TotalLength = reader[17] != DBNull.Value ? Convert.ToInt32(reader[17]) : 0;
                    machineData.ShoulderLength = reader[18] != DBNull.Value ? Convert.ToInt32(reader[18]) : 0;
                    machineData.Startshoulderlength = reader[19] != DBNull.Value ? Convert.ToInt32(reader[19]) : 0;
                    machineData.TipLength = reader[20] != DBNull.Value ? Convert.ToInt32(reader[20]) : 0;
                    machineData.CuttingLength = reader[21] != DBNull.Value ? Convert.ToInt32(reader[21]) : 0;
                    machineData.HLength = reader[22] != DBNull.Value ? Convert.ToInt32(reader[22]) : 0;
                    machineData.Material = reader[23] != DBNull.Value ? (string)reader[23] : "";
                    machineData.UnitsFeedSpin = reader[24] != DBNull.Value ? (string)reader[24] : "";
                    machineData.FType = reader[25] != DBNull.Value ? (string)reader[25] : "";
                    machineData.FeedXY = reader[26] != DBNull.Value ? Convert.ToDecimal(reader[26]) : 0;
                    machineData.FeedZ = reader[27] != DBNull.Value ? Convert.ToDecimal(reader[27]) : 0;
                    machineData.FeedFinish = reader[28] != DBNull.Value ? Convert.ToDecimal(reader[28]) : 0;
                    machineData.SType = reader[29] != DBNull.Value ? (string)reader[29] : "";
                    machineData.Spin = reader[30] != DBNull.Value ? Convert.ToDecimal(reader[30]) : 0;
                    machineData.SpinFinish = reader[31] != DBNull.Value ? Convert.ToDecimal(reader[31]) : 0;
                    machineData.FeedZPenetration = reader[32] != DBNull.Value ? (string)reader[32] : "";
                    machineData.FeedLeadIn = reader[33] != DBNull.Value ? Convert.ToInt32(reader[33]) : 0;
                    machineData.FeedLeadOut = reader[34] != DBNull.Value ? Convert.ToInt32(reader[34]) : 0;
                    machineData.FeedLink = reader[35] != DBNull.Value ? Convert.ToInt32(reader[35]) : 0;
                machineData.ToolName = reader[36] != DBNull.Value ? (string)reader[36] : "";
                machineData.ToolGroup = reader[37] != DBNull.Value ? (string)reader[37] : "";
                machineData.HolderName = reader[38] != DBNull.Value ? (string)reader[38] : "";
                machineData.GroupHolderName = reader[39] != DBNull.Value ? (string)reader[39] : "";
                machineData.Direction = reader[40] != DBNull.Value ? (string)reader[40] : "";
                machineData.ChamferLength = reader[41] != DBNull.Value ? Convert.ToInt32(reader[41]) : 0;
                machineData.TipDiameter = reader[42] != DBNull.Value ? Convert.ToDecimal(reader[42]) : 0;
                machineData.ShoulderDiameter = reader[43] != DBNull.Value ? Convert.ToDecimal(reader[43]) : 0;
                machineData.ShoulderAngle = reader[44] != DBNull.Value ? Convert.ToDecimal(reader[44]) : 0;
                machineData.Message1 = reader[45] != DBNull.Value ? (string)reader[45] : "";
                machineData.Message2 = reader[46] != DBNull.Value ? (string)reader[46] : "";
                machineData.Message3 = reader[47] != DBNull.Value ? (string)reader[47] : "";
                machineData.Message4 = reader[48] != DBNull.Value ? (string)reader[48] : "";
                machineData.Message5 = reader[49] != DBNull.Value ? (string)reader[49] : "";

                machineData.FloodCoolant = reader[50] != DBNull.Value ? Convert.ToInt32(reader[50]) : 0;
                machineData.MistCoolant = reader[51] != DBNull.Value ? Convert.ToInt32(reader[51]) : 0;
                machineData.HighPressureCoolant = reader[52] != DBNull.Value ? Convert.ToInt32(reader[52]) : 0;
                machineData.LowPressureCoolant = reader[53] != DBNull.Value ? Convert.ToInt32(reader[53]) : 0;
                machineData.ThroughHighPressureCoolant = reader[54] != DBNull.Value ? Convert.ToInt32(reader[54]) : 0;
                machineData.ThroughLowPressureCoolant = reader[55] != DBNull.Value ? Convert.ToInt32(reader[55]) : 0;
                machineData.AirBlastCoolant = reader[56] != DBNull.Value ? Convert.ToInt32(reader[56]) : 0;
                machineData.MinimumQuantityLubricationCoolant = reader[57] != DBNull.Value ? Convert.ToInt32(reader[57]) : 0;
                machineData.MinimumQuantityLubricationValue = reader[58] != DBNull.Value ? Convert.ToInt32(reader[58]) : 0;
                machineData.Rough = reader[59] != DBNull.Value ? Convert.ToInt32(reader[59]) : 0;
                machineData.SpinDirection = reader[60] != DBNull.Value ? (string)reader[60] : "";
                machineData.SpinLimit = reader[61] != DBNull.Value ? Convert.ToInt32(reader[61]) : 0;
                machineData.ThreadingStandard = reader[62] != DBNull.Value ? (string)reader[62] : "";
                machineData.ThreadStandardByUser = reader[63] != DBNull.Value ? (string)reader[63] : "";
                machineData.ThreadingTableStandard = reader[64] != DBNull.Value ? (string)reader[64] : "";
                machineData.NumThreads = reader[65] != DBNull.Value ? Convert.ToInt32(reader[65]) : 0;
                machineData.Pitch = reader[66] != DBNull.Value ? Convert.ToDecimal(reader[66]) : 0;
                machineData.PitchUnit = reader[67] != DBNull.Value ? (string)reader[67] : "";
                machineData.CNCName = reader[68] != DBNull.Value ? (string)reader[68] : "";

                machineDataList.Add(machineData);
                }

                reader.Close();
              
                return machineDataList;
            }

            public async Task AddAll(List<Tool> tools)
        {
            foreach (var tool in tools)
            {
               await Add(tool);
            }

        }
            public async Task Add(Tool tool)
            {
                SqliteCommand command =
                    new SqliteCommand(
                        @"INSERT INTO MachineDatadb (IdNumber, Radius, Diameter, Length,CNCName) SELECT @IdNumber, @Radius, @Diameter, @Length,@CNCName
WHERE NOT EXISTS (SELECT 1 FROM MachineDatadb WHERE IdNumber = @IdNumber)",
                        _dbConfig.SqliteConn);



                command.Parameters.AddWithValue("@IdNumber", tool.Id);
                command.Parameters.AddWithValue("@Radius", tool.Radius);
                command.Parameters.AddWithValue("@Diameter", tool.Diameter);
                command.Parameters.AddWithValue("@Length", tool.CoordinateZ);
                command.Parameters.AddWithValue("@CNCName", tool.CNCName);
           



                //kayıt olup olmadı mı kontrolü ? olduysa 1 : olmadıysa 0, return eder.
                await command.ExecuteNonQueryAsync();

        }
        public async Task Update(Tool tool)
            {
               
                SqliteCommand command =
                    new SqliteCommand(
                        "UPDATE  MachineDatadb SET Radius=@radius, Diameter=@diameter, Length=@coordinatez,CNCName=@CNCName WHERE IdNumber=@id2",
                        _dbConfig.SqliteConn);
                command.Parameters.AddWithValue(@"radius", tool.Radius);
                command.Parameters.AddWithValue(@"diameter", tool.Diameter);
                command.Parameters.AddWithValue(@"coordinatez", tool.CoordinateZ);
                command.Parameters.AddWithValue(@"CNCName", tool.CNCName);
                command.Parameters.AddWithValue(@"id2", tool.Id);



                //kayıt olup olmadı mı kontrolü ? olduysa 1 : olmadıysa 0, return eder.
               await command.ExecuteNonQueryAsync();
            



            }
            public async Task Update(MachineData machineData)
            {
               
                SqliteCommand command =
                    new SqliteCommand(
                        "UPDATE  MachineDatadb SET Tool=@Tool,ToolType=@ToolType,ToolUserType=@ToolUserType,Permanent=@Permanent,UnitsDiameter=@UnitsDiameter,Diameter=@Diameter,Angle=@Angle,Radius=@Radius,ProfileRadius=@ProfileRadius,UpperRadius=@UpperRadius,NumTeeth=@NumTeeth,Description=@Description,TaperAngle=@TaperAngle,ShankDiameter=@ShankDiameter,UnitsLength=@UnitsLength,Length=@Length,TotalLength=@TotalLength,ShoulderLength=@ShoulderLength,Startshoulderlength=@Startshoulderlength,TipLength=@TipLength,CuttingLength=@CuttingLength,HLength=@HLength,Material=@Material,UnitsFeedSpin=@UnitsFeedSpin,FType=@FType,FeedXY=@FeedXY,FeedZ=@FeedZ,FeedFinish=@FeedFinish,SType=@SType,Spin=@Spin,SpinFinish=@SpinFinish,FeedZPenetration=@FeedZPenetration,FeedLeadIn=@FeedLeadIn,FeedLeadOut=@FeedLeadOut,FeedLink=@FeedLink,ToolName=@ToolName,ToolGroup=@ToolGroup,HolderName=@HolderName,GroupHolderName=@GroupHolderName,Direction=@Direction,ChamferLength=@ChamferLength,TipDiameter=@TipDiameter,ShoulderDiameter=@ShoulderDiameter,ShoulderAngle=@ShoulderAngle,Message1=@Message1,Message2=@Message2,Message3=@Message3,Message4=@Message4,Message5=@Message5,FloodCoolant=@FloodCoolant,MistCoolant=@MistCoolant,HighPressureCoolant=@HighPressureCoolant,LowPressureCoolant=@LowPressureCoolant,ThroughHighPressureCoolant=@ThroughHighPressureCoolant,ThroughLowPressureCoolant=@ThroughLowPressureCoolant,AirBlastCoolant=@AirBlastCoolant,MinimumQuantityLubricationCoolant=@MinimumQuantityLubricationCoolant,MinimumQuantityLubricationValue=@MinimumQuantityLubricationValue,Rough=@Rough,SpinDirection=@SpinDirection,SpinLimit=@SpinLimit,ThreadingStandard=@ThreadingStandard,ThreadStandardByUser=@ThreadStandardByUser,ThreadingTableStandard=@ThreadingTableStandard,NumThreads=@NumThreads,Pitch=@Pitch,PitchUnit=@PitchUnit,CNCName=@CNCName WHERE @IdNumber=IdNumber",
                        _dbConfig.SqliteConn);
                command.Parameters.AddWithValue("@Tool", machineData.Tool);
                command.Parameters.AddWithValue("@IdNumber", machineData.IdNumber);
                command.Parameters.AddWithValue("@ToolType", machineData.ToolType);
                command.Parameters.AddWithValue("@ToolUserType", machineData.ToolUserType);
                command.Parameters.AddWithValue("@Permanent", machineData.Permanent);
                command.Parameters.AddWithValue("@UnitsDiameter", machineData.UnitsDiameter);
                command.Parameters.AddWithValue("@Diameter", machineData.Diameter);
                command.Parameters.AddWithValue("@Angle", machineData.Angle);
                command.Parameters.AddWithValue("@Radius", machineData.Radius);
                command.Parameters.AddWithValue("@ProfileRadius", machineData.ProfileRadius);
                command.Parameters.AddWithValue("@UpperRadius", machineData.UpperRadius);
                command.Parameters.AddWithValue("@NumTeeth", machineData.NumTeeth);
                command.Parameters.AddWithValue("@Description", machineData.Description);
                command.Parameters.AddWithValue("@TaperAngle", machineData.TaperAngle);
                command.Parameters.AddWithValue("@ShankDiameter", machineData.ShankDiameter);
                command.Parameters.AddWithValue("@UnitsLength", machineData.UnitsLength);
                command.Parameters.AddWithValue("@Length", machineData.Length);
                command.Parameters.AddWithValue("@TotalLength", machineData.TotalLength);
                command.Parameters.AddWithValue("@ShoulderLength", machineData.ShoulderLength);
                command.Parameters.AddWithValue("@Startshoulderlength", machineData.Startshoulderlength);
                command.Parameters.AddWithValue("@TipLength", machineData.TipLength);
                command.Parameters.AddWithValue("@CuttingLength", machineData.CuttingLength);
                command.Parameters.AddWithValue("@HLength", machineData.HLength);
                command.Parameters.AddWithValue("@Material", machineData.Material);
                command.Parameters.AddWithValue("@UnitsFeedSpin", machineData.UnitsFeedSpin);
                command.Parameters.AddWithValue("@FType", machineData.FType);
                command.Parameters.AddWithValue("@FeedXY", machineData.FeedXY);
                command.Parameters.AddWithValue("@FeedZ", machineData.FeedZ);
                command.Parameters.AddWithValue("@FeedFinish", machineData.FeedFinish);
                command.Parameters.AddWithValue("@SType", machineData.SType);
                command.Parameters.AddWithValue("@Spin", machineData.Spin);
                command.Parameters.AddWithValue("@SpinFinish", machineData.SpinFinish);
                command.Parameters.AddWithValue("@FeedZPenetration", machineData.FeedZPenetration);
                command.Parameters.AddWithValue("@FeedLeadIn", machineData.FeedLeadIn);
                command.Parameters.AddWithValue("@FeedLeadOut", machineData.FeedLeadOut);
                command.Parameters.AddWithValue("@FeedLink", machineData.FeedLink);
                command.Parameters.AddWithValue("@ToolName", machineData.ToolName);
                command.Parameters.AddWithValue("@ToolGroup", machineData.ToolGroup);
                command.Parameters.AddWithValue("@HolderName", machineData.HolderName);
                command.Parameters.AddWithValue("@GroupHolderName", machineData.GroupHolderName);
                command.Parameters.AddWithValue("@Direction", machineData.Direction);
                command.Parameters.AddWithValue("@ChamferLength", machineData.ChamferLength);
                command.Parameters.AddWithValue("@TipDiameter", machineData.TipDiameter);
                command.Parameters.AddWithValue("@ShoulderDiameter", machineData.ShoulderDiameter);
                command.Parameters.AddWithValue("@ShoulderAngle", machineData.ShoulderAngle);
                command.Parameters.AddWithValue("@Message1", machineData.Message1);
                command.Parameters.AddWithValue("@Message2", machineData.Message2);
                command.Parameters.AddWithValue("@Message3", machineData.Message3);
                command.Parameters.AddWithValue("@Message4", machineData.Message4);
                command.Parameters.AddWithValue("@Message5", machineData.Message5);
                command.Parameters.AddWithValue("@FloodCoolant", machineData.FloodCoolant);
                command.Parameters.AddWithValue("@MistCoolant", machineData.MistCoolant);
                command.Parameters.AddWithValue("@HighPressureCoolant", machineData.HighPressureCoolant);
                command.Parameters.AddWithValue("@LowPressureCoolant", machineData.LowPressureCoolant);
                command.Parameters.AddWithValue("@ThroughHighPressureCoolant", machineData.ThroughHighPressureCoolant);
                command.Parameters.AddWithValue("@ThroughLowPressureCoolant", machineData.ThroughLowPressureCoolant);
                command.Parameters.AddWithValue("@AirBlastCoolant", machineData.AirBlastCoolant);
                command.Parameters.AddWithValue("@MinimumQuantityLubricationCoolant", machineData.MinimumQuantityLubricationCoolant);
                command.Parameters.AddWithValue("@MinimumQuantityLubricationValue", machineData.MinimumQuantityLubricationValue);
                command.Parameters.AddWithValue("@Rough", machineData.Rough);
                command.Parameters.AddWithValue("@SpinDirection", machineData.SpinDirection);
                command.Parameters.AddWithValue("@SpinLimit", machineData.SpinLimit);
                command.Parameters.AddWithValue("@ThreadingStandard", machineData.ThreadingStandard);
                command.Parameters.AddWithValue("@ThreadStandardByUser", machineData.ThreadStandardByUser);
                command.Parameters.AddWithValue("@ThreadingTableStandard", machineData.ThreadingTableStandard);
                command.Parameters.AddWithValue("@NumThreads", machineData.NumThreads);
                command.Parameters.AddWithValue("@Pitch", machineData.Pitch);
                command.Parameters.AddWithValue("@PitchUnit", machineData.PitchUnit);
                command.Parameters.AddWithValue("@CNCName", machineData.CNCName);


                //kayıt olup olmadı mı kontrolü ? olduysa 1 : olmadıysa 0, return eder.
               await command.ExecuteNonQueryAsync();
            



            }
            public async Task UpdateAll(List<Tool> tools
            )
            {
                foreach (var tool in tools)
                {
                   await Update(tool);
                   await AddAll(tools);
                }

            }
            public async Task UpdateAll(List<MachineData> machineDatas
            )
            {
                foreach (var machineData in machineDatas)
                {
                    await Update(machineData);
                }

            }
            public void Dispose()
            {
                GC.SuppressFinalize(this);
            }
            public decimal GetMachineData(int receivedData)
            {
                string cmdline = $"SELECT * FROM MachineDatadb WHERE IdNumber={receivedData}";
            SqliteCommand command = new SqliteCommand(cmdline, _dbConfig.SqliteConn);

            decimal length;
            SqliteDataReader reader = command.ExecuteReader();
            reader.Read();
            
                length = reader[16] != DBNull.Value ? Convert.ToDecimal(reader[16]) : 0;
                
            
            return length;

        }
        }

}


