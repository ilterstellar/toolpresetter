﻿using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using ToolDataLibraryAsync.Models;
using System.Configuration;
using Microsoft.Data.Sqlite;

namespace ToolDataLibraryAsync.Data_Access.SQLite.SettingsDb
{
    
    public class SettingsDal
    {
        private static readonly string WorkingDirectory = Environment.CurrentDirectory;
        private readonly string _projectDirectory = Directory.GetParent(WorkingDirectory)?.Parent?.FullName;
        private DbConfig _dbConfig;
        private string _updateString;

        public SettingsDal()
        {
            _dbConfig = new DbConfig();

            string configFilePath =
                $"{_projectDirectory}\\App.config";
            ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
            configFileMap.ExeConfigFilename = configFilePath;
            Configuration config =
                ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);
            //_path = $"DataSource={config.AppSettings.Settings["ToolDbPath"].Value}; Version=3";

            
            CreateSettingsTableIfNotExists();
        }
        public void UpdateAllSettings(UISettings settings)
        {
            SqliteCommand command =
                new SqliteCommand(
                    "UPDATE  UISettings SET FontSize = @FontSize, Style=@Style, SerialPort=@SerialPort, Language=@Language, TimeInterval=@TimeInterval WHERE UISettings=@UISettings",
                    _dbConfig.SqliteConn);

            command.Parameters.AddWithValue("@UISettings", "UISettings");
            command.Parameters.AddWithValue("@FontSize", settings.FontSize);
            command.Parameters.AddWithValue("@Style", settings.Style);
            command.Parameters.AddWithValue("@SerialPort", settings.Port);
            command.Parameters.AddWithValue("@Language", settings.Language);
            command.Parameters.AddWithValue("@TimeInterval", settings.TimeInterval);




            //kayıt olup olmadı mı kontrolü ? olduysa 1 : olmadıysa 0, return eder.
            command.ExecuteNonQuery();




        }
        public async Task CreateSettingsTableIfNotExists()
        {
            using (SqliteCommand command = new SqliteCommand(
                "CREATE TABLE IF NOT EXISTS UISettings (" +
                "UISettings TEXT PRIMARY KEY, " +
                "FontSize INTEGER, " +
                "Style TEXT, " +
                "SerialPort TEXT, " +
                "Language TEXT, " +
                "TimeInterval INTEGER,"+
                "HostName TEXT," +
                "Port INTEGER," +
                "MQPort INTEGER)",
                _dbConfig.SqliteConn))
            {
                command.ExecuteNonQuery();

                // Check if the table was just created
                bool tableJustCreated = await IsTableJustCreated("UISettings");

                if (tableJustCreated)
                {
                    // Table was just created, insert default settings
                    await CreateDefaultSettings();
                }
            }
        }

        private async Task<bool> IsTableJustCreated(string tableName)
        {
            using (SqliteCommand command = new SqliteCommand(
                $"SELECT name FROM sqlite_master WHERE type='table' AND name='{tableName}'",
                _dbConfig.SqliteConn))
            {
                var result = await command.ExecuteScalarAsync();
                return result != null && result.ToString().Equals(tableName, StringComparison.OrdinalIgnoreCase);
            }
        }

        public async Task<bool> SettingsTableExists()
        {
            SqliteCommand command = new SqliteCommand(
                "SELECT name FROM sqlite_master WHERE type='table' AND name='UISettings'",
                _dbConfig.SqliteConn);

            object result = await command.ExecuteScalarAsync();

            return result != null && result.ToString() == "UISettings";
        }

        public async Task CreateDefaultSettings()
        {
            UISettings defaultSettings = new UISettings
            {
                uISettings = "UISettings",
                FontSize = 12,
                Style = "White",
                SerialPort = "COM6",
                Language = "en-US",
                TimeInterval = 10,
                HostName="localhost",
                Port=12345,
                MQPort=5672
            };

            SqliteCommand command = new SqliteCommand(
                "INSERT INTO UISettings (UISettings, FontSize, Style, SerialPort, Language, TimeInterval,HostName,Port,MQPort) " +
                "VALUES (@UISettings, @FontSize, @Style, @SerialPort, @Language, @TimeInterval,@HostName,@Port,@MQPort)",
                _dbConfig.SqliteConn);

            command.Parameters.AddWithValue("@UISettings", defaultSettings.uISettings);
            command.Parameters.AddWithValue("@FontSize", defaultSettings.FontSize);
            command.Parameters.AddWithValue("@Style", defaultSettings.Style);
            command.Parameters.AddWithValue("@SerialPort", defaultSettings.SerialPort);
            command.Parameters.AddWithValue("@Language", defaultSettings.Language);
            command.Parameters.AddWithValue("@TimeInterval", defaultSettings.TimeInterval);
            command.Parameters.AddWithValue("@HostName", defaultSettings.HostName);
            command.Parameters.AddWithValue("@Port", defaultSettings.Port);
            command.Parameters.AddWithValue("@MQPort", defaultSettings.MQPort);

            await command.ExecuteNonQueryAsync();
        }

    

public async Task<UISettings> GetAllSettings()
        {



            SqliteCommand command = new SqliteCommand("SELECT * FROM UISettings", _dbConfig.SqliteConn);
            SqliteDataReader reader = command.ExecuteReader();

            reader.Read();

            var settings = new UISettings()
            {
                FontSize = Convert.ToInt32(reader["FontSize"]),
                Style = reader["Style"].ToString(),
                SerialPort = reader["SerialPort"].ToString(),
                Language = reader["Language"].ToString(),
                TimeInterval = Convert.ToInt32(reader["TimeInterval"]),
                HostName = reader["HostName"].ToString(),
                Port = Convert.ToInt32(reader["Port"]),
                MQPort = Convert.ToInt32(reader["MQPort"])
            };
            ;


            reader.Close();
            await command.ExecuteNonQueryAsync();

            return settings;
        }
        object _setting;
        public Object GetSetting(string column)
        {

            SqliteCommand command = new SqliteCommand($"SELECT {column} FROM UISettings WHERE UISettings=@UISettings", _dbConfig.SqliteConn);
            command.Parameters.AddWithValue("@UISettings", "UISettings");
            SqliteDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                _setting = reader[column].ToString();
            }
            reader.Close();

            return _setting;
        }
        public void UpdateSetting(string column, object selection)
        {
            string query = "UPDATE UISettings SET " + column + "=@value WHERE UISettings=@UISettings";

            using (var command = new SqliteCommand(query, _dbConfig.SqliteConn))
            {
                command.Parameters.AddWithValue("@value", value:selection.ToString());
                command.Parameters.Add("@UISettings", (SqliteType)DbType.String).Value = "UISettings";

                command.ExecuteNonQuery();
            }
        }

       
    }
}
