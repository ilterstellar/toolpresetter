﻿
using Microsoft.Data.Sqlite;
using System;

using System.IO;
using System.Windows.Threading;

namespace ToolDataLibraryAsync.Data_Access
{
    public class DbConfig
    {
        private static readonly string WorkingDirectory = Environment.CurrentDirectory;
        public static DbConfig Instance { get; set; } = new DbConfig();
        public SqliteConnection SqliteConn { get; set; } = null;
        public SqliteConnection SqliteConnect { get; set; } = null;
   
        public SqliteConnection SqliteConnection(string path)
        {
            SqliteConnect = new SqliteConnection($"DataSource={path};");
            SqliteConnect.Open();

            Dispatcher.CurrentDispatcher.ShutdownStarted += CurrentDispatcher_ShutdownStarted;

            return SqliteConnect;
        }
        public DbConfig()
        {

             var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                                "ToolPresetter");
            var path2 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                   "ToolPresetter","Data Files");
            System.IO.Directory.CreateDirectory(path);
            System.IO.Directory.CreateDirectory(path2);
            var dbPath = $"{path}" +"\\"+ "toolpresetter.db";
            SqliteConn = new SqliteConnection($"DataSource={dbPath};");
            SqliteConn.Open();
            Dispatcher.CurrentDispatcher.ShutdownStarted += CurrentDispatcher_ShutdownStarted;
        }
        private void CurrentDispatcher_ShutdownStarted(object sender, System.EventArgs e)
        {
            if (SqliteConn != null)
            {
                SqliteConn.Close();
                SqliteConn.Dispose();
                SqliteConn = null;
            }

            if (SqliteConnect != null)
            {
                SqliteConnect.Close();
                SqliteConnect.Dispose();
                SqliteConnect = null;
            }
        }
    }
}
